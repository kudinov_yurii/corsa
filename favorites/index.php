<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Избранное");

$obFavorites = new \Favorites\FavoritesManager();
$favList = $obFavorites->getIdList();
?>
<?if (count($favList)>0){?>
	<?$GLOBALS['arrFavsList'] = array("ID"=>$favList);?>
	<?$APPLICATION->IncludeComponent("bitrix:news.list","models",Array(
		"FILTER_NAME" => "arrFavsList",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "",
		"IBLOCK_ID" => '1',
		"NEWS_COUNT" => "40",
		"SORT_BY1" => "CATALOG_PRICE_1",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FIELD_CODE" => Array("CATALOG_PRICE_1"),
		"PROPERTY_CODE" => Array(""),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	)
	);?>
<?} else {?>
	<div class="wrapper">
		<h2 class="">Список избранного пуст</h2>
	</div>	
<?}?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>