<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
$model = htmlspecialchars($request->getQuery("model")); 
if ( !empty($model) ) {
	CModule::IncludeModule("iblock");
	$modelAR = '';
	$res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>1, "ACTIVE"=>"Y", "CODE"=>$model), false, false, array("PROPERTY_AR_MODEL_LINK") );
	if($ob = $res->Fetch()){
		$modelAR = $ob["PROPERTY_AR_MODEL_LINK_VALUE"];
	}
}
?>
<!doctype html>
<html>
  <head>
    <title>Примерить в интерьере</title>
  </head>
  <body>
	
	<?if ( !empty($modelAR) ){?>
		<style type="text/css">
			.viewer {width:409px; height:441px; position:absolute; top:50%; left:50%; margin-right:-50%; transform:translate(-50%, -50%);}
		</style>
		<script src="https://ar.elarbis.com/common/model-viewer.js"></script>
			
		<model-viewer class="viewer" id="viewer" poster="https://ar.elarbis.com/common/ar_fit.png" 
		src="https://ar.elarbis.com/corsastyle/assets/<?=$modelAR?>.glb" 
		ios-src="https://ar.elarbis.com/corsastyle/assets/<?=$modelAR?>.usdz"> 
		</model-viewer>
	<?}?>
			
  </body>
</html>
