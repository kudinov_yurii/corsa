<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
<div class="row">
<div class="col-md-8 col-lg-7 offset-lg-4">

<div style="font-size:20px;">
<p>8 (800) 707 25 04</p>
<p><?$APPLICATION->IncludeFile(SITE_DIR."include/email.php", Array(), Array("SHOW_BORDER"=>true));?></p>
<p style="max-width:600px;"><script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A018196b9c031f2c47bebbc18fc9064d8801ecac3aec87cea8052928eacb68f1d&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script></p>

<p><b>Режим работы:</b> ПН-ПТ, с 9:00 до 18:00</p>

<p><b>Производство:</b>
<?$APPLICATION->IncludeFile(SITE_DIR."include/address.php", Array(), Array("SHOW_BORDER"=>true));?>, 
<?$APPLICATION->IncludeFile(SITE_DIR."include/phone.php", Array(), Array("SHOW_BORDER"=>true));?><br>
</p>

<h3>Полное наименование юр. лица и ИНН:</h3>
<p>ООО "МЕБЕЛЬНАЯ ФАБРИКА", ИНН: 6311186630</p>

<h3><span class="toggle-link" data-toggler="requisites">Реквизиты</span></h3>
<div data-toggle-text="requisites">
ООО "МЕБЕЛЬНАЯ ФАБРИКА"<br>
Юр. адрес: 443030, Россия, Самарская область, г. Самара, ул. Речная, д. 5, пом. 1<br>
Почт. адрес: 443030, Россия, Самарская область, г. Самара, ул. Речная, д. 5, пом. 1<br>
ОГРН: 1196313086903<br>
ИНН: 6311186630<br>
КПП: 631101001<br>
Банк: ФИЛИАЛ ТОЧКА ПАО БАНКА «ФК ОТКРЫТИЕ»<br>
БИК банка: 044525999<br>
Расчетный счет: 40702810914500023300<br>
Кор.счет: 30101810845250000999<br>
Тел: +7 846 2110204
</div>

</div>

</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>