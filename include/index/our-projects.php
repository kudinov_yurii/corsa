<div class="wrapper wrapper-full">
	<div class="our-projects">
		<div class="vert-strip"></div>
	
		<div class="our-projects-img"><img src="<?=SITE_TEMPLATE_PATH?>/i/logo-big.png" alt=""></div>
		<div class="our-projects-text">
			<h2 class="section-title interior-title">Другие наши проекты</h2>
			<p>Фабрика мебели “CorsaStyle” с 2011 года занимается производством дизайнерском мебели для сегмента HORECA. В нашем ассортименте вы найдете стулья, кресла, диваны, для ресторанов и кафе, баров и клубов, отелей, офисов, и также для частных пространств. За время работы компания стала лидером в изготовлении мебели для коммерческих заведений в Самарском регионе и заняла одни из ведущих позиций в Поволжье.</p>
			<a style="margin-top:0;" class="interior-index-btn" href="https://corsastyle.ru/" target="_blank" rel="nofollow">подробнее</a>
		</div>
	</div>
</div>