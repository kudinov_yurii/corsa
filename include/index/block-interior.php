<div class="wrapper wrapper-full">
	<div class="interior-index">
		<div class="interior-index-text">
			<h2 class="section-title interior-title text-left">Примерить в интерьере</h2>
			<p>Вам не нужно гадать, как будет смотреться выбранная модель стула вашем доме. С помощью он-лайн примерки вы сможете поместить понравившийся стул в собственный интерьер.</p>
			<p>Все модели можно  поворачивать, перемещать и рассматривать с различных ракурсов.</p>
			<a class="interior-index-btn" href="/catalog/">подробнее</a>
		</div>
	</div>
</div>