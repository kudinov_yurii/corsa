<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
define('NO_AGENT_CHECK', true);
define("STOP_STATISTICS", true);

// Pасчет доставки Magic Trans

/* получаем список городов */
if ($_REQUEST["action"]=="getCityList") {

	$curlRes = curlConnect('dictionary/getCityList');
	echo json_encode($curlRes);
	
}

/* расчет стоимости доставки между городами */
if ($_REQUEST["action"]=="calculate") {
	
	$params = array(
		//"from" => $_REQUEST["from"],
		"from" => "17322", //samara
		"to" => $_REQUEST["to"],
		"items" => $_REQUEST["items"]
	);

	$curlRes = curlConnect('delivery/calculate', $params);

	echo json_encode($curlRes);
	
}

/* получение параметров для доставки по городу назначения. */
if ($_REQUEST["action"]=="cityDelivery") {
	
	$params = array(
		//"from" => $_REQUEST["from"],
		"from" => "17322", //samara
		"to" => $_REQUEST["to"],
	);

	$curlRes = curlConnect('delivery/CityDelivery', $params);
	$arValues = reset($curlRes["result"]["to"]);
	
	// стоимость доставки в зависимости от объема
	if ( $_REQUEST["volume"]>0 && $_REQUEST["volume"]<0.3 ){
		$arKey = "PROPERTY_V_03";
	} elseif ( $_REQUEST["volume"]>0.3 && $_REQUEST["volume"]<0.5 ){
		$arKey = "PROPERTY_V_05";
	} elseif ( $_REQUEST["volume"]>0.5 && $_REQUEST["volume"]<1 ){
		$arKey = "PROPERTY_V_1";
	} elseif ( $_REQUEST["volume"]>1 && $_REQUEST["volume"]<2 ){
		$arKey = "PROPERTY_V_2";
	} elseif ( $_REQUEST["volume"]>2 && $_REQUEST["volume"]<4 ){
		$arKey = "PROPERTY_V_4";
	} elseif ( $_REQUEST["volume"]>4 && $_REQUEST["volume"]<8 ){
		$arKey = "PROPERTY_V_8";
	} elseif ( $_REQUEST["volume"]>8 && $_REQUEST["volume"]<12 ){
		$arKey = "PROPERTY_V_12";
	} elseif ( $_REQUEST["volume"]>12 && $_REQUEST["volume"]<14 ){
		$arKey = "PROPERTY_V_14";
	} elseif ( $_REQUEST["volume"]>14 && $_REQUEST["volume"]<16 ){
		$arKey = "PROPERTY_V_16";
	} elseif ( $_REQUEST["volume"]>16 && $_REQUEST["volume"]<30 ){
		$arKey = "PROPERTY_V_30";
	} elseif ( $_REQUEST["volume"]>30 && $_REQUEST["volume"]<35 ){
		$arKey = "PROPERTY_V_35";
	} elseif ( $_REQUEST["volume"]>35 && $_REQUEST["volume"]<40 ){
		$arKey = "PROPERTY_V_40";
	} elseif ( $_REQUEST["volume"]>40 && $_REQUEST["volume"]<86 ){
		$arKey = "PROPERTY_V_86";
	}

	echo json_encode( $arValues[$arKey] );
	
	
}

?>