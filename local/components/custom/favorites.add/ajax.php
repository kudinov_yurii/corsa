<?
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$obResponse = new stdClass();

$obRequest = \Bitrix\Main\Context::getCurrent()->getRequest();

$sActionName = $obRequest->get('action');
$elementId = $obRequest->get('element_id');
$obResponse->action = $sActionName;
$obResponse->message = '';
$obResponse->code = array();


if (!empty($elementId) && (int)$elementId > 0 ) {

	$obFavorites = new \Favorites\FavoritesManager();
	
	if ($sActionName == "add") {
		$res = $obFavorites->add($elementId);
		$obResponse->res = $res;
		$obResponse->message = 'add to favorites';
		$obResponse->code = 200;
	}
	
	if ($sActionName == "remove") {
		$res = $obFavorites->remove($elementId);
		$obResponse->res = $res;
		$obResponse->message = 'remove form favorites';
		$obResponse->code = 200;
	}

	$obResponse->count = count($obFavorites->getIdList());
}

header('Content-Type: application/json');
echo json_encode($obResponse);
?>