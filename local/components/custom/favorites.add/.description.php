<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("FAVORITES_NAME"),
	"DESCRIPTION" => GetMessage("FAVORITES_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"SORT" => 160,
	"PATH" => array(
		"ID" => "dev",
		"NAME" => GetMessage("DEV_NAME"),
		"CHILD" => array(
			"ID" => "favorites",
			"NAME" => GetMessage("FAVORITES_NAME")
		)
	),
);
?>