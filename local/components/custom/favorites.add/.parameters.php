<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = array(
	"GROUPS" => array(
		"GENERAL_SETTINGS" => array(
			"NAME" => GetMessage("GENERAL_SETTINGS"),
		),
	),
	"PARAMETERS" => array(
			"ELEMENT_ID" => Array(
			"PARENT"=>"GENERAL_SETTINGS",
			"NAME" => GetMessage("ELEMENT_ID"), 
			"TYPE" => "STRING",
			"DEFAULT" => "",
			
		),	
	)
);
?>