<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$bFavorite = false;
if(!empty($arResult['ITEMS'])){
	if(in_array($arParams['ELEMENT_ID'],$arResult['ITEMS'])){
		$bFavorite = true;
	}
}
?>

<?if($arParams['ELEMENT_ID'] > 0){?>
	<a 
		class="<?=($arParams["LIST"]=="Y" ? "product-item-favorite" : "product-favorite")?> <?= $bFavorite ? '-in' : '';?>" 
		data-toggle="favorite" 
		data-element="<?=$arParams['ELEMENT_ID']?>" 
		data-action="<?= $bFavorite ? 'remove' : 'add';?>" 
		title="<?= $bFavorite ? 'Удалить из избранного' : 'Добавить в избранное';?>" 
		href="#">
	</a>
<?}?>