
$(document).on('click','[data-toggle="favorite"]', function(){
	var __this = $(this);
	var action = $(this).attr('data-action');
	var element_id = __this.data('element');
		
	$.post(
		'/local/components/custom/favorites.add/ajax.php', 
		{
			action : action,
			element_id : element_id
		}, 
		function(json){

			console.log(json);
			if(json.code == 200){
				var buttons = $('[data-toggle="favorite"][data-element="'+element_id+'"]')
				if(action === 'remove'){
					buttons.removeClass('-in');
					buttons.attr('data-action','add');
					buttons.attr('title','Добавить в избранное');
				} else {
					buttons.addClass('-in');
					buttons.attr('data-action','remove');
					buttons.attr('title','Удалить из избранного');
				}
		}
		
	});
	return false;
});