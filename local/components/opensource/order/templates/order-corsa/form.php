<?php

use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Application;


if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var OpenSourceOrderComponent $component */

$order = $component->order; 
?>
<form action="" method="post" name="os-order-form" id="os-order-form">
	<input type="hidden" name="person_type_id" value="<?= $arParams['PERSON_TYPE_ID'] ?>">
	
	<div data-total-volume="<?=$arResult['TOTAL_VOLUME_M3']?>"></div>
	<?// список товаров для расчета доставки?>
	<?foreach ($arResult['BASKET'] as $basketItem){?>
		<div class="delivery-items" data-weight="<?=$basketItem["WEIGHT_KG"]?>" data-volume="<?=$basketItem["VOLUME_M3"]?>"></div>
	<?}?>
		
	<div class="order-step active" data-step="1">

		<h1 class="h1 text-center">Рассчитайте доставку</h1>
		<?$APPLICATION->IncludeFile(SITE_DIR."include/order-steps.php", Array("DELIVERY" => "Y"), Array("SHOW_BORDER"=>false));?>

		<div class="delivery">

		
			<div class="delivery-subtitle">Как вы хотите получить свой заказ <!--в <span>г.Самара</span--></div>
			<? foreach ($arResult['DELIVERY_ERRORS'] as $error):
					/** @var Error $error */
					?>
					<div class="error"><?= $error->getMessage() ?></div>
			<? endforeach; ?>
			
			<div class="order-radio-list" id="os-order-delivery-block">
				<? foreach ($arResult['DELIVERY_LIST'] as $arDelivery): ?>
				<div class="order-radio-item">
					<label>
						<input type="radio" name="delivery_id" <?= $arDelivery['CHECKED'] ? 'checked' : '' ?> value="<?= $arDelivery['ID'] ?>" data-delivery-type="<?= $arDelivery['ID'] ?>">
						<div class="order-radio-item--inner">
							<?if ($arDelivery['LOGO_PATH']){?>
								<img class="order-radio-item--img" src="<?=$arDelivery['LOGO_PATH']?>" alt="">
							<?}?>
							<div class="order-radio-item--name"><?= $arDelivery['NAME'] ?> <?//= $arDelivery['PRICE_DISPLAY'] ?></div>
						</div>
					</label>
				</div>
				<? endforeach; ?>
			</div>
			
			<div class="order-form">
			<?php foreach ($arResult['PROPERTIES'] as $propCode => $arProp): ?>
				<? foreach ($arProp['ERRORS'] as $error):
						/** @var Error $error */
						?>
						<div class="error"><?= $error->getMessage() ?></div>
				<? endforeach; ?>
				<?php
				switch ($arProp['TYPE']):
						case 'LOCATION':
								?>
								<div class="location">
									<select class="location-search" name="<?= $arProp['FORM_NAME'] ?>"id="<?= $arProp['FORM_LABEL'] ?>">
											<option data-data='<? echo Json::encode($arProp['LOCATION_DATA']) ?>'	value="<?= $arProp['VALUE'] ?>"><?= $arProp['LOCATION_DATA']['label'] ?></option>
									</select>
								</div>
								<?
								break;
						default:
							$fieldType = ( $arProp['FORM_LABEL']=="property_EMAIL" ? "email" : "text" );
								?>
								
								<input class="order-field <?if (empty($arProp['VALUE'])){?>field-novalid<?}?>" id="<?= $arProp['FORM_LABEL'] ?>" type="<?=$fieldType?>"
											<?if ($arProp["FORM_LABEL"]=="property_CITY") {?>data-delivery-to<?}?>
											 name="<?= $arProp['FORM_NAME'] ?>"
											 value="<?= $arProp['VALUE'] ?>"
											 placeholder="<?=($arProp['DESCRIPTION'] ? : $arProp['NAME'] )?>">
						<? endswitch; ?>

			<? endforeach; ?>
			</div>
			
			<div>
			<? foreach ($arResult['DELIVERY_LIST'] as $arDelivery): ?>
				<div class="delivery-type-block" data-delivery-type="<?= $arDelivery['ID'] ?>">
					
					<?if ($arDelivery["STORE"]){?>
						<div class="delivery-point">
							<span class="delivery-point-name">1 пункт выдачи в г. Самара</span>
							<span class="delivery-point-time"><?=$arDelivery["STORE"]["SCHEDULE"]?></span>
						</div>
						<div class="delivery-point-map"><?=$arDelivery["STORE"]["DESCRIPTION"]?></div>
					<?}?>
					
					<?if ($arDelivery['ID']==1 || $arDelivery['ID']==17){?>
					<div class="basket-discount-block">
						Доставка: <span class="delivery-sum" id="delivery-sum-<?=$arDelivery['ID']?>">Рассчитать стоимость доставки поможет наш менеджер</span>
						<div class="basket-discount-block--note">(доставка оплачивается при получении заказа)</div>
					</div>
					<?}?>
				
				</div>
				
			<? endforeach; ?>

			</div>
			
			<div class="basket-checkout-block">Итого к оплате: <span><b><?=number_format($arResult['SUM'],0,'.',' '); ?> рублей</b></span></div>
			<div class="basket-btns-block">
				<a class="btn btn-block" href="#" data-step-to="2">подтвердить заказ</a>
				<a class="btn btn-gray btn-block" href="<?=$arParams["PATH_TO_BASKET"]?>">вернуться в корзину</a>
			</div>
			
		</div><!-- /.delivery -->
		
		
	</div>	

	<div class="order-step " data-step="2">

		<h1 class="h1 text-center">Оплата</h1>
		<?$APPLICATION->IncludeFile(SITE_DIR."include/order-steps.php", Array("PAYMENT" => "Y"), Array("SHOW_BORDER"=>false));?>
		<div class="payment">
	
			<? foreach ($arResult['PAY_SYSTEM_ERRORS'] as $error):
					/** @var Error $error */
					?>
					<div class="error"><?= $error->getMessage() ?></div>
			<? endforeach; ?>
		
			<div class="order-radio-list">
				<? foreach ($arResult['PAY_SYSTEM_LIST'] as $arPaySystem): ?>
				<div class="order-radio-item">
					<label>
						<input type="radio" name="pay_system_id" <?= $arPaySystem['CHECKED'] ? 'checked' : '' ?> value="<?= $arPaySystem['ID'] ?>" data-payment-type="<?= $arPaySystem['ID'] ?>">
						<div class="order-radio-item--inner">
							<?if ($arPaySystem['LOGOTIP']){?>
							<img class="order-radio-item--img" src="<?=CFile::getPath($arPaySystem['LOGOTIP'])?>" alt="">
							<?}?>
							<div class="order-radio-item--name"><?= $arPaySystem['NAME'] ?></div>
						</div>
					</label>
				</div>
				<? endforeach; ?>
			</div>
			
			<div class="basket-btns-block">
				<input type="hidden" name="save" value="y">
				<button class="btn btn-block" type="submit">подтвердить заказ</button>
				<a class="btn btn-gray btn-block" href="#" data-step-to="1">вернуться к доставке</a>
			</div>
			
		</div><!-- /.payment -->
		
	</div>	
<?/*
<h3><?= Loc::getMessage('OPEN_SOURCE_ORDER_TEMPLATE_DELIVERY_PRICES_TITLE') ?>:</h3>
<table>
		<tr>
				<td><?= Loc::getMessage('OPEN_SOURCE_ORDER_TEMPLATE_DELIVERY_BASE_PRICE') ?></td>
				<td><?= $arResult['DELIVERY_BASE_PRICE_DISPLAY'] ?></td>
		</tr>
		<tr>
				<td><?= Loc::getMessage('OPEN_SOURCE_ORDER_TEMPLATE_DELIVERY_PRICE') ?></td>
				<td><?= $arResult['DELIVERY_PRICE_DISPLAY'] ?></td>
		</tr>
		<tr>
				<td><?= Loc::getMessage('OPEN_SOURCE_ORDER_TEMPLATE_DELIVERY_DISCOUNT') ?></td>
				<td><?= $arResult['DELIVERY_DISCOUNT_DISPLAY'] ?></td>
		</tr>
</table>

<h3><?= Loc::getMessage('OPEN_SOURCE_ORDER_TEMPLATE_SUM_TITLE') ?>:</h3>
<table>
		<tr>
				<td><?= Loc::getMessage('OPEN_SOURCE_ORDER_TEMPLATE_TOTAL_BASE_PRICE') ?></td>
				<td><?= $arResult['SUM_BASE_DISPLAY'] ?></td>
		</tr>
		<tr>
				<td><?= Loc::getMessage('OPEN_SOURCE_ORDER_TEMPLATE_TOTAL_DISCOUNT') ?></td>
				<td><?= $arResult['DISCOUNT_VALUE_DISPLAY'] ?></td>
		</tr>
		<tr>
				<td><?= Loc::getMessage('OPEN_SOURCE_ORDER_TEMPLATE_TOTAL_PRICE') ?></td>
				<td><?= $arResult['SUM_DISPLAY'] ?></td>
		</tr>
</table>
*/?>

</form>