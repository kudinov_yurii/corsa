<?php

use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Order;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var OpenSourceOrderComponent $component */

$order = $component->order; 
?>
<?//= Loc::getMessage('OPEN_SOURCE_ORDER_TEMPLATE_ORDER_CREATED', ['#ORDER_ID#' => $arResult['ID'] ]) ?>

<?if ($arResult["PAY_SYSTEM_ID"]=="4"){ //оплата картами?>

	<h1 class="h1 text-center">Оплата</h1>
	<?$APPLICATION->IncludeFile(SITE_DIR."include/order-steps.php", Array("PAYMENT" => "Y"), Array("SHOW_BORDER"=>false));?>
	<div class="payment">
		<?
		if ($order) {
			$paymentId = $arResult['ID'];
			
			$paymentCollection = $order->getPaymentCollection();
			
			if ($paymentCollection) {
				if ($paymentId) {
					$paymentItem = $paymentCollection->getItemById($paymentId);
				}

				if ($paymentItem !== null) {
					$service = \Bitrix\Sale\PaySystem\Manager::getObjectById($paymentItem->getPaymentSystemId());
					if ($service) {
						$context = \Bitrix\Main\Application::getInstance()->getContext();

						if ($returnUrl) {
							$service->getContext()->setUrl($returnUrl);
						}

						$result = $service->initiatePay($paymentItem, $context->getRequest());

						if (!$result->isSuccess()) {
							echo implode('<br>', $result->getErrorMessages());
						}

					}
				}
				
			} //paymentCollection
			
		} //order
		?>
	</div>
<?} else {?>

	<?$APPLICATION->IncludeFile(SITE_DIR."include/order-steps.php", Array("DONE" => "Y"), Array("SHOW_BORDER"=>false));?>

	<div class="order-done">

		<div class="text-center"><img src="<?=SITE_TEMPLATE_PATH?>/i/order-done.jpg" alt=""></div>
		<h1 class="h1 text-center">Спасибо за покупку</h1>
		<div class="order-done-text">В течении 40 минут наш менеджер<br> свяжется с вами для подтверждения заказа</div>

		<div class="basket-btns-block">
			<a class="btn btn-block" href="/">перейти на главную</a>
		</div>
		

	</div>
<?}?>
<script data-skip-moving="true">
	fbq('track', 'Purchase');
	console.log('fbq Purchase');
</script>

