$(document).ready(function () {
	
	//////////////
	/* $(function() {
		
		var result = $('.delivery-sum');
		var resultToHome = $('#delivery-sum-1');
		var inputTo = $('input[data-delivery-to]');
		var fromCityId = "17322"; //samara
		var toCityId = 0;
		var cities = [];
		var freeDelivery = false;
		var messDelivery = {
			free : 'бесплатная доставка',
			unknown : 'стоимость доставки уточняйте у наших менеджеров'
		}

		$.post('/local/ajax/delivery_mt.php', {action : "getCityList"}, function(response) {
			if ($.isPlainObject(response)){
				if (response.status === true){
					var l = response.result.length,
							i = -1;

					while(++i < l){
						cities.push({
							id: response.result[i].id,
							value: response.result[i].name
						});
					}

					inputTo.autocomplete({
						lookup: cities,
						onSelect: function(suggestion) {
							console.log(suggestion);
							toCityId = suggestion.id;
							freeDelivery = ( suggestion.id==17322 ? true : false );
							
							
								result.empty();
								$deliverySum = 0;

								var data = {
									action : "calculate",
									from: fromCityId,
									to: toCityId,
									items: [],
								};

								$('.delivery-items').each(function() {
									data.items.push({
										weight: $(this).data('weight'),
										volume: $(this).data('volume'),
									});
								});

								$.post('/local/ajax/delivery_mt.php', data, function(response) {
									console.log(response);
									
									if ($.isPlainObject(response)) {
										if (response.status === true) {
											if ( freeDelivery ){
												result.html(messDelivery.free);
											} else if ( response.result.price === 0 ){
												result.html(messDelivery.unknown);
											} else {
												result.html( parseInt(response.result.price) + " руб." );
												$deliverySum = parseInt(response.result.price);
											}
										} else {
											//result.text(response.message);
											result.html(messDelivery.unknown);
										}
									}
								}, 'json');
								
								var data1 = {
									action : "cityDelivery",
									from: fromCityId,
									to: toCityId,
									volume: $('[data-total-volume]').data('total-volume'),
								};
								
								setTimeout(function() { 
									$.post('/local/ajax/delivery_mt.php', data1, function(response1) {
										console.log(response1);
										console.log($deliverySum);
										if ($deliverySum > 0 && response1 != null){
											resultToHome.html( parseInt(response1) + $deliverySum + " руб." );
										}
									}, 'json');
								}, 300);


							
							
						},
						onHint: function (hint) {
							//console.log(hint);
						},
						onInvalidateSelection: function() {
							console.log('You selected: none');
						}

					});

				}
			}
		}, 'json');



	}); */
	//////////////
	
	
		$('input[name="delivery_id"]').on('change', function() {
			if ($(this).val() == 16){//пункт выдачи
			
				$('#property_ADDRESS, #property_CITY').hide();
				$('#property_ADDRESS, #property_CITY').removeClass('field-novalid');
				
			} else if ($(this).val() == 17){
				$('#property_ADDRESS').hide();
				$('#property_ADDRESS').removeClass('field-novalid');
				$('#property_CITY').show();
				if ($('#property_CITY').val() !=''){
					$('#property_CITY').removeClass('field-novalid');
				} else {
					$('#property_CITY').addClass('field-novalid');
				} 
				
			} else {
				
				$('#property_ADDRESS, #property_CITY').show();
				$('#property_ADDRESS, #property_CITY').each(function () {
					if ($(this).val() !=''){
						$(this).removeClass('field-novalid');
					} else {
						$(this).addClass('field-novalid');
					}
				});
				
			}
			
		});
		
		$('.order-field').on('change', function() { 
			if ($(this).val() !=''){
				$(this).removeClass('field-novalid');
			} else {
				$(this).addClass('field-novalid');
			}
		});
		
		$('[data-step-to]').on('click', function() {

			$(this).closest('.order-step').find('.order-field').each(function () {
				if ( $(this).hasClass('field-novalid') ){
					$(this).addClass('error-novalid');
				} else {
					$(this).removeClass('error-novalid');
				}
			});
			
			var novalid = $(this).closest('.order-step').find('.order-field.error-novalid').length;
			console.log( novalid );
			if (novalid==0){
				var step = $(this).data('step-to');
				$('.order-step').hide();
				$('.order-step[data-step="'+step+'"]').show();
				
				$('html, body').animate({
					scrollTop: $('.order-step').offset().top
				}, 300);
			}
			
			return false;
		});
	
    var $locationSearch = $('.location-search');

		setSelectize();
		BX.addCustomEvent('onAjaxSuccess', function(){
			setSelectize();
		});
		
		function setSelectize() {
			var $locationSearch = $('.location-search');
			$locationSearch.selectize({
					valueField: 'code',
					labelField: 'label',
					searchField: 'label',
					create: false,
					render: {
							option: function (item, escape) {
									return '<div class="title">' + escape(item.label) + '</div>';
							}
					},
					load: function (q, callback) {
							if (!q.length) return callback();

							var query = {
									c: 'opensource:order',
									action: 'searchLocation',
									mode: 'ajax',
									q: q
							};

							$.ajax({
									url: '/bitrix/services/main/ajax.php?' + $.param(query, true),
									type: 'GET',
									error: function () {
											callback();
									},
									success: function (res) {
											if (res.status === 'success') {
													callback(res.data);
											} else {
													console.log(res.errors);
													callback();
											}
									}
							});
					}
			});
		}

    /**
     * Recalculate all deliveries on location prop change
     */
    var $deliveryBlock = $('#os-order-delivery-block');

    function renderDelivery(deliveryData) {
			//console.log(deliveryData);
        var html = '';
        html += '<div class="order-radio-item"><label class="delivery-label delivery' + deliveryData.id + '">';
        html += '<input type="radio" name="delivery_id" value="' + deliveryData.id + '" data-delivery-type="' + deliveryData.id + '"> ';
        html += '<div class="order-radio-item--inner"> ';
				html += '<img class="order-radio-item--img" src="' + deliveryData.logo_path + '"> ';
        html += '<div class="order-radio-item--name">'+ deliveryData.name +'</div>';
				html += '</div> ';
        //html += ', <span class="delivery-price">' + deliveryData.price_display + '</span>';
        html += '</label></div>';

        $deliveryBlock.append(html);
    }

    $locationSearch.change(function (event) {
        var locationCode = $(event.target).val();

        if (locationCode.length > 0) {
            var formData = $('#os-order-form').serialize();

            var query = {
                c: 'opensource:order',
                action: 'calculateDeliveries',
                mode: 'ajax'
            };

            var request = $.ajax({
                url: '/bitrix/services/main/ajax.php?' + $.param(query, true),
                type: 'POST',
                data: formData
            });

            request.done(function (result) {
                var selectedDelivery = $deliveryBlock.find('input:checked');
                var selectedDeliveryId = 0;
                if(selectedDelivery.length > 0) {
                    selectedDeliveryId = selectedDelivery.val();
                }

                $deliveryBlock.html('');
                $.each(result.data, function (i, deliveryData) {
                    renderDelivery(deliveryData);
                });

                if(selectedDeliveryId > 0) {
                    $deliveryBlock.find('label.delivery' + selectedDeliveryId + ' input').prop('checked', true);
                }
            });

            request.fail(function () {
                console.error('Can not get delivery data');
            });
        }
    });
});
