<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Page\Asset; 
CJSCore::Init();

$curPage = $APPLICATION->GetCurPage();
$isIndex = ($APPLICATION->GetCurPage() == '/');
$showWrapper = (($APPLICATION->GetDirProperty("SHOW_WRAPPER") == "Y") ? true : false);
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowHead();?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">

	<link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/i/favicon.png"/>
	<!--link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic" /-->

	<?
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/js/fancybox/jquery.fancybox.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/js/slick/slick.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/bootstrap-reboot.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/bootstrap-grid.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/styles.css");
	
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles_temp.css"); //временный

	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery-3.5.1.min.js");	
	?>
	<script>var templatePath = '<?=SITE_TEMPLATE_PATH?>';</script>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script data-skip-moving="true" async src="https://www.googletagmanager.com/gtag/js?id=G-2FG6N35PWR"></script>
	<script data-skip-moving="true">
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-2FG6N35PWR');
	</script>
	
	<!-- Facebook Pixel Code -->
	<script data-skip-moving="true">
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '353725919171510');
	fbq('track', 'PageView');
	</script>
	<!-- End Facebook Pixel Code -->
	
</head>
<body <? if ($USER->IsAuthorized()) echo ' class="-authorized"';?>>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	
<header class="header" id="header">
	<div class="wrapper container-fluid">
		<div class="header-cols">
			<span class="nav-left-toggle"></span>
			<a href="tel:<?$APPLICATION->IncludeFile(SITE_DIR."include/phone.php", Array(), Array("SHOW_BORDER"=>false));?>" class="header-phone-icon"></a>
			<div class="h-col-logo">
				<a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/i/logo-new.png"></a>
			</div>
			<div class="h-col-nav">
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu", 
				"top", 
				array(
					"ROOT_MENU_TYPE" => "top",
					"CHILD_MENU_TYPE" => "child",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "36000",
					"MENU_CACHE_USE_GROUPS" => "N",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MAX_LEVEL" => "1",
					"USE_EXT" => "Y",
					"ALLOW_MULTI_SELECT" => "N",
					"COMPONENT_TEMPLATE" => "",
					"DELAY" => "N"
				),
				false
				);
			 ?>
			</div>
			<div class="h-col-icons">
				<div class="header-icons">
					<a class="header-icon header-icon-search" href="javascript:void(0);"><img src="<?=SITE_TEMPLATE_PATH?>/i/svg/loupe.svg" alt=""></a>
					<a class="header-icon" href="/favorites/"><img src="<?=SITE_TEMPLATE_PATH?>/i/svg/heart.svg" alt=""></a>
					<a class="header-icon" href="/personal/cart/"><img src="<?=SITE_TEMPLATE_PATH?>/i/svg/cart.svg" alt=""></a>
				</div>
			</div>
		</div>
	</div>
</header><!-- /.header -->

<form class="header-search-form" action="/search/">
	<input class="header-search-input" type="text" name="q" value="" placeholder="Поиск">
	<input class="header-search-btn" name="s" type="submit" value="">
</form>

<div class="nav-left">
	<div>
		<ul class="list-simple nav-left-list">
			<li class="nav-left-item">
				<a class="nav-left-link" href="/catalog/chairs/">Стулья</a>
				<?$GLOBALS['arrModelsFilter'] = array("!ID"=>13790);?>
				<?$APPLICATION->IncludeComponent("bitrix:news.list","nav-left-list",Array(
					"FILTER_NAME" => "arrModelsFilter",
					"DISPLAY_NAME" => "Y",
					"AJAX_MODE" => "N",
					"IBLOCK_TYPE" => "",
					"IBLOCK_ID" => '1',
					"NEWS_COUNT" => "40",
					"SORT_BY1" => "CATALOG_PRICE_1",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FIELD_CODE" => Array(""),
					"PROPERTY_CODE" => Array(""),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_LAST_MODIFIED" => "Y",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "N",
					"PAGER_TITLE" => "",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"SET_STATUS_404" => "N",
					"SHOW_404" => "N",
					"MESSAGE_404" => "",
					"PAGER_BASE_LINK" => "",
					"PAGER_PARAMS_NAME" => "arrPager",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => ""
				)
				);?>
			</li>
			<li class="nav-left-item"><a class="nav-left-link" href="#">Собери свой стул</a></li>
			<li class="nav-left-item"><a class="nav-left-link" href="#">примерить в интерьере</a></li>
			<li class="nav-left-item"><a class="nav-left-link" href="#">сми о нас</a></li>
			<li class="nav-left-item"><a class="nav-left-link" href="#">стать дилером</a></li>
			<li class="nav-left-item"><a class="nav-left-link" href="#">дизайнерам</a></li>
		</ul>
	</div>
	<?$APPLICATION->IncludeComponent("bitrix:news.list","nav-left-models",Array(
		"DISPLAY_NAME" => "Y",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "",
		"IBLOCK_ID" => '1',
		"NEWS_COUNT" => "40",
		"SORT_BY1" => "CATALOG_PRICE_1",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FIELD_CODE" => Array("CATALOG_PRICE_1"),
		"PROPERTY_CODE" => Array("CATEGORY"),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
		)
	);?>
</div><!-- /.nav-left -->


<div class="main">

	<?if (!$isIndex){?>
		<div class="wrapper container-fluid">
			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "crumbs", Array(
				"START_FROM" => "0",
				"PATH" => "",
				"SITE_ID" => "-",
			),
			false
		);?>
		<?$APPLICATION->AddBufferContent('ShowH1');?>
		</div>	
	<?}?>

	<?if ($showWrapper){?>
		<div class="wrapper page-content">
	<?}?>
