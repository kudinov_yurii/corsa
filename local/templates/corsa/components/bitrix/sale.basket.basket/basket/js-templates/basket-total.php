<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */
?>
<script id="basket-total-template" type="text/html">
	<div class="" data-entity="basket-checkout-aligner">
	
		<div class="basket-panel">
			<?if ($arParams['HIDE_COUPON'] !== 'Y'){?>
				<div class="basket-coupon-block">
					<input class="basket-coupon-input" type="text" value="" placeholder="Введите промокод" data-entity="basket-coupon-input">
					<span class="basket-coupon-btn"></span>
				</div>
			<?}?>
			<div class="basket-total-block">Итого: <span><b>{{{PRICE_WITHOUT_DISCOUNT_FORMATED}}}</b></span></div>
		</div>
		
		<?if ($arParams['HIDE_COUPON'] !== 'Y'){?>
			<div class="basket-coupon-alert-section">
					{{#COUPON_LIST}}
					<div class="basket-coupon-alert text-{{CLASS}}">
						<span class="basket-coupon-text">
							<strong>{{COUPON}}</strong> - <?=Loc::getMessage('SBB_COUPON')?> {{JS_CHECK_CODE}}
							{{#DISCOUNT_NAME}}({{DISCOUNT_NAME}}){{/DISCOUNT_NAME}}
						</span>
						<span class="close-link" data-entity="basket-coupon-delete" data-coupon="{{COUPON}}">
							<?=Loc::getMessage('SBB_DELETE')?>
						</span>
					</div>
					{{/COUPON_LIST}}
			</div>
		<?}?>
		
		{{#DISCOUNT_PRICE_FORMATED}}
		<div class="basket-discount-block">Скидка: <span>{{{DISCOUNT_PRICE_FORMATED}}}</span></div>
		{{/DISCOUNT_PRICE_FORMATED}}
		<div class="basket-checkout-block">Итого к оплате: <span><b data-entity="basket-total-price">{{{PRICE_FORMATED}}}</b></span></div>
		
		<div class="basket-btns-block">
			<button class="btn btn-block {{#DISABLE_CHECKOUT}} disabled{{/DISABLE_CHECKOUT}}" data-entity="basket-checkout-button"><?=Loc::getMessage('SBB_ORDER')?></button>
			<a class="btn btn-gray btn-block" href="/catalog/">продолжить покупки</a>
		</div>


	</div>
</script>