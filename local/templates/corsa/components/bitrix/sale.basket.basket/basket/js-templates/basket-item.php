<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $mobileColumns
 * @var array $arParams
 * @var string $templateFolder
 */

?>

<script id="basket-item-template" type="text/html">
	<tr class="basket-table-item{{#SHOW_RESTORE}} d-none{{/SHOW_RESTORE}}" id="basket-item-{{ID}}" data-entity="basket-item" data-id="{{ID}}">
			<td>
				<img class="basket-item-img" alt="{{NAME}}" src="{{{IMAGE_URL}}}{{^IMAGE_URL}}<?=$templateFolder?>/images/no_photo.png{{/IMAGE_URL}}">
			</td>
				<td>
					<div class="basket-item-info">
						<span class="basket-item-delete d-md-none"></span>
						{{#COLUMN_LIST}}
							<div class="basket-item-category" data-column-property-code="{{CODE}}">{{VALUE}}</div>
						{{/COLUMN_LIST}}
						<div class="basket-item-name" data-entity="basket-item-name">{{NAME}}</div>

						<div class="basket-item-block-properties">
						
						{{#PROPS}}
							<div class="basket-item-material" data-property-code="{{CODE}}"><span style="background-image: url({{PICT}});"></span> {{{NAME}}} - {{{VALUE}}}</div>
						{{/PROPS}}

						</div>
					</div>

			</td>

			<td>
				<div class="basket-item-count{{#NOT_AVAILABLE}} disabled{{/NOT_AVAILABLE}}" data-entity="basket-item-quantity-block">
					<span data-entity="basket-item-quantity-minus">-</span>
					<input type="text" value="{{QUANTITY}}" {{#NOT_AVAILABLE}} disabled="disabled"{{/NOT_AVAILABLE}} data-value="{{QUANTITY}}" data-entity="basket-item-quantity-field" id="basket-item-quantity-{{ID}}">
					<span data-entity="basket-item-quantity-plus">+</span>
				</div>
			</td>

			<td>
				<span id="basket-item-sum-price-{{ID}}">
					{{{SUM_PRICE_FORMATED}}}
				</span>
			</td>

			<td>
				<span class="basket-item-delete d-none d-md-inline-block" data-entity="basket-item-delete"></span>
			</td>

	</tr>
</script>