<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS']
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
	'STICKER_ID' => $mainId.'_sticker',
	'BIG_SLIDER_ID' => $mainId.'_big_slider',
	'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId.'_slider_cont',
	'OLD_PRICE_ID' => $mainId.'_old_price',
	'PRICE_ID' => $mainId.'_price',
	'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
	'PRICE_TOTAL' => $mainId.'_price_total',
	'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
	'QUANTITY_ID' => $mainId.'_quantity',
	'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
	'QUANTITY_UP_ID' => $mainId.'_quant_up',
	'QUANTITY_MEASURE' => $mainId.'_quant_measure',
	'QUANTITY_LIMIT' => $mainId.'_quant_limit',
	'BUY_LINK' => $mainId.'_buy_link',
	'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
	'COMPARE_LINK' => $mainId.'_compare_link',
	'TREE_ID' => $mainId.'_skudiv',
	'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
	'OFFER_GROUP' => $mainId.'_set_group_',
	'BASKET_PROP_DIV' => $mainId.'_basket_prop',
	'SUBSCRIBE_LINK' => $mainId.'_subscribe',
	'TABS_ID' => $mainId.'_tabs',
	'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
	'TABS_PANEL_ID' => $mainId.'_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);

$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers) {
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);

	foreach ($arResult['OFFERS'] as $offer) {
		if ($offer['MORE_PHOTO_COUNT'] > 1) {
			break;
		}
	}
} else {
	$actualItem = $arResult;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$showDiscount = $price['PERCENT'] > 0;

$arProps = $arResult["PROPERTIES"];
$productImg = ($arResult["DETAIL_PICTURE"]["SRC"] ? : $arResult["PREVIEW_PICTURE"]["SRC"]);
?>
<span data-img-src="<?=$productImg?>"></span>
<div class="product" id="<?=$itemIds['ID']?>">
	<div class="wrapper container-fluid">
		<div class="row">
			<div class="col-md-7 col-lg-8">
				<div class="product-gallery">

					<div class="product-favorite-mobile d-block d-md-none">
						<? //favorites 
						if( Bitrix\Main\Loader::includeModule('api.uncachedarea') ) {
								CAPIUncachedArea::includeFile(
									 "/favorites/include.php",
									 array(
											'ID' => $arResult['ID'],
									 )
								);
						}?>
					</div>

					<?if ($arResult["GALLERY"]){?>
					<div class="product-gallery-left">
						<div class="product-slider">
							<?foreach ($arResult["GALLERY"] as $photo){?>
								<div><img src="<?=$photo["medium"]["src"]?>" alt="<?=$alt?>"></div>
							<?}?>
						</div>
					</div>
					<div class="product-gallery-right">
						<div class="product-slider-preview <?if (count($arResult["GALLERY"])<4){?>no-transform<?}?>">
							<?foreach ($arResult["GALLERY"] as $photo){?>
								<div><img src="<?=$photo["thumb"]["src"]?>" alt="<?=$alt?>"></div>
							<?}?>
						</div>
					</div>
					<?} else {?>
						<div class="product-img">
							<img src="<?=$productImg?>" alt="<?=$alt?>">
						</div>
					<?}?>

				</div>
			</div>
			<div class="col-md-5 col-lg-4 col-xl-3">
				<div class="product-left">
					<div class="product-title-block">
						<div class="product-category"><?=$arProps["CATEGORY"]["VALUE"]?></div>
						<div class="product-title"><?=$arResult["NAME"]?></div>
					</div>

					<div class="d-none d-md-block">
						<? //favorites 
						if( Bitrix\Main\Loader::includeModule('api.uncachedarea') ) {
								CAPIUncachedArea::includeFile(
									 "/favorites/include.php",
									 array(
											'ID' => $arResult['ID'],
									 )
								);
						}?>
					</div>
					
					<?if ($arResult['PREVIEW_TEXT']){?>
						<div class="product-preview product-text-hidden" data-id="preview"><?=$arResult['PREVIEW_TEXT']?></div>
						<div><span class="product-text-show" data-id="preview">Подробнее</span></div>
					<?}?>


					<div class="product-price-holder">
						<?if ($arParams['SHOW_OLD_PRICE'] === 'Y'){?>
							<span class="product-price-old" id="<?=$itemIds['OLD_PRICE_ID']?>" style="display: <?=($showDiscount ? '' : 'none')?>;">
							<?=($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '')?></span>
						<?}?>
						<span class="product-price" id="<?=$itemIds['PRICE_ID']?>"><?=number_format($price['RATIO_PRICE'],0,'.',' ');?></span><span class="product-price-rub">рублей</span>
					</div>
					<?if ($haveOffers && !empty($arResult['OFFERS_PROP'])){?>
						<div id="<?=$itemIds['TREE_ID']?>">
							<?foreach ($arResult['SKU_PROPS'] as $skuProperty){
								if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
									continue;

								$propertyId = $skuProperty['ID'];
								$skuProps[] = array(
									'ID' => $propertyId,
									'SHOW_MODE' => $skuProperty['SHOW_MODE'],
									'VALUES' => $skuProperty['VALUES'],
									'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
								);

								if ($skuProperty['CODE']=="UPHOLSTERY"){
									$skuPropertyTitle = 'Выберите обивку';
									$tooltipText = 'Обивка добавлена';
								} elseif ($skuProperty['CODE']=="LEGS") {
									$skuPropertyTitle = 'Выберите ножки';
									$tooltipText = 'Ножки добавлены';
								}
								?>
								<div class="js-modal" data-modal="<?=toLower($skuProperty['CODE'])?>" data-entity="sku-line-block">
									<div class="js-modal-inner modal-big">
										<div class="row align-items-center">
											<!--div class="col-lg-3 d-none d-lg-block text-center">
												<img src="<?=$productImg?>" alt="">
											</div-->
											<div class="--col-lg-8 --offset-lg-1 col-12">
												<div class="choose-upholstery-title"><?=$skuPropertyTitle?></div>
												<ul class="upholstery-list">
													<?foreach ($skuProperty['VALUES'] as &$value) {
														$value['NAME'] = htmlspecialcharsbx($value['NAME']);
													?>
													<?if ($skuProperty['CODE']=="UPHOLSTERY"){?>
														<?if ($value['ID']==1){?>
															<div class="choose-upholstery-subtitle">Коллекция Diamond i Zizi</div>
														<?}?>
														<?if ($value['ID']==23){?>
															<div class="choose-upholstery-subtitle">Коллекция Happy</div>
														<?}?>
														<?if ($value['ID']==50){?>
															<div class="choose-upholstery-subtitle">Коллекция Velutto</div>
														<?}?>
													<?}?>
													<li class="upholstery-item" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>" data-img="<?=$value['PICT']['SRC']?>" data-name="<?=$value['NAME']?>" data-type="<?=toLower($skuProperty['CODE'])?>" data-tooltip="<?=$tooltipText?>">
														<span class="upholstery-item-img" style="background-image:url(<?=$value['PICT']['SRC']?>)"></span>
														<span class="upholstery-item-name"><?=$value['NAME']?></span>
													</li>
													<?}?>
												</ul>
											</div>
										</div>
									</div>
								</div>
							<?}?>
						</div>

						<div class="product-materials-title">Изменить материал</div>
						<div class="product-materials-list">
							<div class="product-material-item js-modal-link" data-modal="upholstery">
								<span class="product-material-img" data-type="upholstery"></span>
								<span class="product-material-type">обивка</span>
							</div>
							<div class="product-material-item js-modal-link" data-modal="legs">
								<span class="product-material-img" data-type="legs"></span>
								<span class="product-material-type">ножки</span>
							</div>
						</div>
					<?}?>




					<div class="product-btns">
						<div id="<?=$itemIds['BASKET_ACTIONS_ID']?>" style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;">
							<button class="btn btn-block btn-basket" <?if ($haveOffers){?>disabled<?}?> type="submit" id="<?=$itemIds['ADD_BASKET_LINK']?>" onclick="fbq('track', 'AddToCart'); console.log('fbq AddToCart');" data-tooltip="Выберите обивку и ножки">В корзину</button>
						</div>
						<?if ($arProps["AR_MODEL_LINK"]["VALUE"]){?>
							<a class="btn btn-gray btn-block" href="/ar/?model=<?=$arResult["CODE"]?>" target="_blank">Примерить в интерьере</a>
						<?}?>
						<?if ($arProps["MODEL_3D_LINK"]["VALUE"]){?>
							<a class="btn btn-gray btn-block" href="<?=$arProps["MODEL_3D_LINK"]["VALUE"]?>" target="_blank" rel="nofollow">Посмотреть 360</a>
						<?}?>
					</div>

					<div class="product-warranty-title">Гарантия 18 месяцев</div>
					<div class="product-warranty-text product-text-hidden" data-id="warranty">Мебель должна служить долго. И её износостойкость обычно проверяется в первый год использования. Но мы знаем, что она будет служить намного дольше, поэтому мы даём на всю нашу мебель увеличенную гарантию — 18 месяцев.</div>
					<div><span class="product-text-show" data-id="warranty">Подробнее</span></div>

				</div>
			</div>
		</div>
	</div>


	<?if (!empty($arResult["CONFIGURATIONS"])){?>
	<div class="section-configuration">
		<div class="wrapper">
			<div class="section-title text-left text-sm-center">Выберете конфигурацию</div>
			<div class="configuration-list">
				<?foreach ($arResult["CONFIGURATIONS"] as $arItem){?>
					<a class="configuration-item" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PICTURE"]["src"]?>" alt=""></a>
				<?}?>
			</div>
		</div>
	</div>
	<?}?>

	<div class="wrapper">
		<div class="product-info">
			<div class="product-info-block">
				<div class="product-info-name">Подробнее о товаре <i></i></div>
				<div class="row product-info-inner">
					<div class="col-md-6">

						<div class="row">
							<?if ($arProps["WIDTH"]["VALUE"] || $arProps["DEPTH"]["VALUE"] || $arProps["HEIGHT"]["VALUE"]){?>
							<div class="col-6">
								<div class="info-prop-title">размер</div>
								<p>Ширина: <?=$arProps["WIDTH"]["VALUE"]?> мм</p>
								<p>Глубина: <?=$arProps["DEPTH"]["VALUE"]?> мм</p>
								<p>Высота: <?=$arProps["HEIGHT"]["VALUE"]?> мм</p>
							</div>
							<?}?>
							<?if ($arProps["WEIGHT"]["VALUE"]){?>
							<div class="col-6 order-last order-md-2">
								<div class="info-prop-title">вес</div>
								<p><?=$arProps["WEIGHT"]["VALUE"]?></p>
							</div>
							<?}?>
							<div class="col-6 order-3">
								<div class="info-prop-title">материал</div>
								<p>Обивка: Велюр</p>
								<p>Ножки: Массив бука</p>
							</div>
						</div>

					</div>
					<div class="col-md-6">
						<?if($arProps["MODEL_SIZE"]["VALUE"]){?>
							<img class="lazy" data-src="<?=CFile::getPath($arProps["MODEL_SIZE"]["VALUE"])?>" alt="">
						<?}?>
					</div>
				</div>
			</div>
			<div class="product-info-block">
				<div class="product-info-name">Упаковка и доставка <i></i></div>
				<div class="row product-info-inner">
					<div class="col-md-5">
						<p>Мы пакуем изделия в специальные коробки так, чтобы они доехали в целости и сохранности.</p>
						<p>Мы отправим стулья в любой регион России и стран СНГ.Срок доставки составляет примерно 1-5 дней, в зависимости от удалённости вашего города или населённого пункта.</p>
						<p>Точная стоимость будет зависит от точного места назначения и количества заказанных стульев.</p>
						<!--div class="product-info-name">Размер упаковки</div>
						<p>Ширина: 640 мм</p>
						<p>Глубина: 640 мм</p>
						<p>Высота: 960 мм</p-->
					</div>
					<div class="col-md-6 offset-md-1">
						<?if($arProps["MODEL_BOX_SIZE"]["VALUE"]){?>
							<img class="lazy" data-src="<?=CFile::getPath($arProps["MODEL_BOX_SIZE"]["VALUE"])?>" alt="">
						<?}?>
					</div>
				</div>
			</div>
		</div>
	</div>


	<?if (!empty($arProps["INTERIOR_PHOTO"]["VALUE"])){?>
	<div class="section-interior">
		<div class="wrapper wrapper-full">
			<div class="section-title"><?=$arProps["CATEGORY"]["VALUE"]?> <?=$arResult["NAME"]?> в интерьере</div>
			<div class="interior-list">
				<?foreach ($arProps["INTERIOR_PHOTO"]["VALUE"] as $k => $photoID){?>
					<?if ($k!=0){
						$w = ($k%3==1 ? "-w-2" : "-w-1");
					}?>
					<a class="interior-item <?=$w?> lazy" data-src="<?=CFile::getPath($photoID)?>" data-fancybox="gallery" href="<?=CFile::getPath($photoID)?>"></a>
				<?}?>
			</div>
		</div>
	</div>
	<?}?>


</div><!-- /.product -->

<?/* if (!empty($arResult['DISPLAY_PROPERTIES'])){
	foreach ($arResult['DISPLAY_PROPERTIES'] as $property){
		<?=$property['NAME']?>
		<?=(is_array($property['DISPLAY_VALUE'])
				? implode(' / ', $property['DISPLAY_VALUE'])
				: $property['DISPLAY_VALUE'])?>
		<?
	}
	unset($property);
	?>
<?} */?>
<?///////
if ($haveOffers)
{
	$offerIds = array();
	$offerCodes = array();

	$useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';

	foreach ($arResult['JS_OFFERS'] as $ind => &$jsOffer)
	{
		$offerIds[] = (int)$jsOffer['ID'];
		$offerCodes[] = $jsOffer['CODE'];

		$fullOffer = $arResult['OFFERS'][$ind];
		$measureName = $fullOffer['ITEM_MEASURE']['TITLE'];

		$strAllProps = '';
		$strMainProps = '';
		$strPriceRangesRatio = '';
		$strPriceRanges = '';

		if ($arResult['SHOW_OFFERS_PROPS'])
		{
			if (!empty($jsOffer['DISPLAY_PROPERTIES']))
			{
				foreach ($jsOffer['DISPLAY_PROPERTIES'] as $property)
				{
					$current = '<dt>'.$property['NAME'].'</dt><dd>'.(
						is_array($property['VALUE'])
							? implode(' / ', $property['VALUE'])
							: $property['VALUE']
						).'</dd>';
					$strAllProps .= $current;

					if (isset($arParams['MAIN_BLOCK_OFFERS_PROPERTY_CODE'][$property['CODE']]))
					{
						$strMainProps .= $current;
					}
				}

				unset($current);
			}
		}

		if ($arParams['USE_PRICE_COUNT'] && count($jsOffer['ITEM_QUANTITY_RANGES']) > 1)
		{
			$strPriceRangesRatio = '('.Loc::getMessage(
					'CT_BCE_CATALOG_RATIO_PRICE',
					array('#RATIO#' => ($useRatio
							? $fullOffer['ITEM_MEASURE_RATIOS'][$fullOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']
							: '1'
						).' '.$measureName)
				).')';

			foreach ($jsOffer['ITEM_QUANTITY_RANGES'] as $range)
			{
				if ($range['HASH'] !== 'ZERO-INF')
				{
					$itemPrice = false;

					foreach ($jsOffer['ITEM_PRICES'] as $itemPrice)
					{
						if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
						{
							break;
						}
					}

					if ($itemPrice)
					{
						$strPriceRanges .= '<dt>'.Loc::getMessage(
								'CT_BCE_CATALOG_RANGE_FROM',
								array('#FROM#' => $range['SORT_FROM'].' '.$measureName)
							).' ';

						if (is_infinite($range['SORT_TO']))
						{
							$strPriceRanges .= Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
						}
						else
						{
							$strPriceRanges .= Loc::getMessage(
								'CT_BCE_CATALOG_RANGE_TO',
								array('#TO#' => $range['SORT_TO'].' '.$measureName)
							);
						}

						$strPriceRanges .= '</dt><dd>'.($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']).'</dd>';
					}
				}
			}

			unset($range, $itemPrice);
		}

		$jsOffer['DISPLAY_PROPERTIES'] = $strAllProps;
		$jsOffer['DISPLAY_PROPERTIES_MAIN_BLOCK'] = $strMainProps;
		$jsOffer['PRICE_RANGES_RATIO_HTML'] = $strPriceRangesRatio;
		$jsOffer['PRICE_RANGES_HTML'] = $strPriceRanges;
	}

	$templateData['OFFER_IDS'] = $offerIds;
	$templateData['OFFER_CODES'] = $offerCodes;
	unset($jsOffer, $strAllProps, $strMainProps, $strPriceRanges, $strPriceRangesRatio, $useRatio);

	$jsParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => true,
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
			'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
			'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
			'OFFER_GROUP' => $arResult['OFFER_GROUP'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
			'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
			'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
			'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
			'USE_STICKERS' => true,
			'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
			'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
			'ALT' => $alt,
			'TITLE' => $title,
			'MAGNIFIER_ZOOM_PERCENT' => 200,
			'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
			'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
			'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
				? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
				: null
		),
		'PRODUCT_TYPE' => $arResult['PRODUCT']['TYPE'],
		'VISUAL' => $itemIds,
		'DEFAULT_PICTURE' => array(
			'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
			'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
		),
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'ACTIVE' => $arResult['ACTIVE'],
			'NAME' => $arResult['~NAME'],
			'CATEGORY' => $arResult['CATEGORY_PATH']
		),
		'BASKET' => array(
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'BASKET_URL' => $arParams['BASKET_URL'],
			'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		),
		'OFFERS' => $arResult['JS_OFFERS'],
		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
		'TREE_PROPS' => $skuProps
	);
}
else
{
	$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
	if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !$emptyProductProperties)
	{
		?>
		<div id="<?=$itemIds['BASKET_PROP_DIV']?>" style="display: none;">
			<?
			if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
			{
				foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propId => $propInfo)
				{
					?>
					<input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=htmlspecialcharsbx($propInfo['ID'])?>">
					<?
					unset($arResult['PRODUCT_PROPERTIES'][$propId]);
				}
			}

			$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
			if (!$emptyProductProperties)
			{
				?>
				<table>
					<?
					foreach ($arResult['PRODUCT_PROPERTIES'] as $propId => $propInfo)
					{
						?>
						<tr>
							<td><?=$arResult['PROPERTIES'][$propId]['NAME']?></td>
							<td>
								<?
								if (
									$arResult['PROPERTIES'][$propId]['PROPERTY_TYPE'] === 'L'
									&& $arResult['PROPERTIES'][$propId]['LIST_TYPE'] === 'C'
								)
								{
									foreach ($propInfo['VALUES'] as $valueId => $value)
									{
										?>
										<label>
											<input type="radio" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]"
												value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"checked"' : '')?>>
											<?=$value?>
										</label>
										<br>
										<?
									}
								}
								else
								{
									?>
									<select name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]">
										<?
										foreach ($propInfo['VALUES'] as $valueId => $value)
										{
											?>
											<option value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"selected"' : '')?>>
												<?=$value?>
											</option>
											<?
										}
										?>
									</select>
									<?
								}
								?>
							</td>
						</tr>
						<?
					}
					?>
				</table>
				<?
			}
			?>
		</div>
		<?
	}

	$jsParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => !empty($arResult['ITEM_PRICES']),
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
			'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
			'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
			'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
			'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
			'USE_STICKERS' => true,
			'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
			'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
			'ALT' => $alt,
			'TITLE' => $title,
			'MAGNIFIER_ZOOM_PERCENT' => 200,
			'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
			'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
			'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
				? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
				: null
		),
		'VISUAL' => $itemIds,
		'PRODUCT_TYPE' => $arResult['PRODUCT']['TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'ACTIVE' => $arResult['ACTIVE'],
			'PICT' => reset($arResult['MORE_PHOTO']),
			'NAME' => $arResult['~NAME'],
			'SUBSCRIPTION' => true,
			'ITEM_PRICE_MODE' => $arResult['ITEM_PRICE_MODE'],
			'ITEM_PRICES' => $arResult['ITEM_PRICES'],
			'ITEM_PRICE_SELECTED' => $arResult['ITEM_PRICE_SELECTED'],
			'ITEM_QUANTITY_RANGES' => $arResult['ITEM_QUANTITY_RANGES'],
			'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
			'ITEM_MEASURE_RATIOS' => $arResult['ITEM_MEASURE_RATIOS'],
			'ITEM_MEASURE_RATIO_SELECTED' => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
			'MAX_QUANTITY' => $arResult['PRODUCT']['QUANTITY'],
			'STEP_QUANTITY' => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
			'CATEGORY' => $arResult['CATEGORY_PATH']
		),
		'BASKET' => array(
			'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		)
	);
	unset($emptyProductProperties);
}

?>
<script>
	BX.message({
		ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
		TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
		TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
		BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
		BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
		BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
		BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
		BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
		TITLE_SUCCESSFUL: '<?=GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
		COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
		COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
		COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
		BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
		PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
		PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
		RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
		RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
		SITE_ID: '<?=CUtil::JSEscape($component->getSiteId())?>'
	});

	var <?=$obName?> = new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
</script>
<?
unset($actualItem, $itemIds, $jsParams);