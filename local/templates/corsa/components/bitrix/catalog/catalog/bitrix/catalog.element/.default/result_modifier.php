<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

if (!empty($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"])){
	$arResult["GALLERY"] = array();

	$imgSizeThumb  = array('width'=>160,  'height'=>200);
	$imgSizeMedium = array('width'=>600, 'height'=>800);

	foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $fotoId) {
		$mediumImg = CFile::ResizeImageGet($fotoId, $imgSizeMedium,	BX_RESIZE_IMAGE_PROPORTIONAL, true);
		$thumbImg = CFile::ResizeImageGet($fotoId, $imgSizeThumb, BX_RESIZE_IMAGE_EXACT, true);
		$arResult["GALLERY"][] = array(
			'thumb' => $thumbImg,
			'medium'=> $mediumImg,
		);
	}
}

if (!empty($arResult["PROPERTIES"]["CONFIGURATIONS"]["VALUE"])){
	$arResult["CONFIGURATIONS"] = array();
	$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL");
	$arFilter = Array("IBLOCK_ID"=> $arParams["IBLOCK_ID"], "ACTIVE"=>"Y", "ID"=>$arResult["PROPERTIES"]["CONFIGURATIONS"]["VALUE"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNext()) {
		if ($ob["PREVIEW_PICTURE"])
			$ob["PICTURE"] = CFile::ResizeImageGet($ob["PREVIEW_PICTURE"], array('width'=>285, 'height'=>285), BX_RESIZE_IMAGE_EXACT, false);
		$arResult["CONFIGURATIONS"][] = $ob;
	}
}
?>
