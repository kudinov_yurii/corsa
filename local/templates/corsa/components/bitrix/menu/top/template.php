<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<?if (!empty($arResult)) { ?>
	<span class="nav-toggle"></span>
	<ul class="list-simple navigation-list">
		<?foreach($arResult as $arItem) {?>
			<?if ($arItem["DEPTH_LEVEL"] > 1) continue; ?>
			<?$active = ($arItem["SELECTED"] ? "active" : "");?>
				<li class="navigation-item">
					<a class="navigation-link <?=$active?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
				</li>
				<?/* if($arItem["CHILD_KEYS"]) {?>
					<ul class="subnavigation-list list-simple">
						<?foreach($arItem["CHILD_KEYS"] as $cKey) {?>
							<?$subItem = $arResult[$cKey];?>
								<li class="subnavigation-item">
									<a class="subnavigation-link" href="<?=$subItem["LINK"]?>"><?=$subItem["TEXT"]?></a>
								</li>
						<?}?>
					</ul>
				<?} */?>
		<?}?>
	</ul>
<?}?>