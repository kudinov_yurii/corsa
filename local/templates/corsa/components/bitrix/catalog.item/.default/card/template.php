<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
$arProps = $item["PROPERTIES"];
if ($item['PREVIEW_PICTURE']['SRC']){
	$imgThumb = CFile::ResizeImageGet($item['PREVIEW_PICTURE']['ID'], array('width'=>500,  'height'=>500), BX_RESIZE_IMAGE_EXACT, true);
}
?>
	<span id="<?=$itemIds['PICT_SLIDER']?>" style="display: none;"></span>
	
	<div class="product-item-img">
		<? //favorites 
		if( Bitrix\Main\Loader::includeModule('api.uncachedarea') ) {
				CAPIUncachedArea::includeFile(
					 "/favorites/include.php",
					 array(
							'ID' => $item['ID'],
							'LIST' => "Y",
					 )
				);
		}?>
		<?if ($arProps["CATALOG_IMAGE"]["VALUE"]){?>
			<div class="img-normal" style="background-image:url(<?=CFile::GetPath($arProps["CATALOG_IMAGE"]["VALUE"])?>);"></div>
		<?}?>
		<?if ($arResult['ORDER']==0){?>
			<?if ($arProps["CATALOG_IMAGE_HOR"]["VALUE"]){?>
				<div class="img-x2" style="background-image:url(<?=CFile::GetPath($arProps["CATALOG_IMAGE_HOR"]["VALUE"])?>);"></div>
			<?}?>
		<?}?>
		<?/* if ($arProps["CATALOG_IMAGE_VERT"]["VALUE"]){?>
			<div class="img-x2-v" style="background-image:url(<?=CFile::GetPath($arProps["CATALOG_IMAGE_VERT"]["VALUE"])?>);"></div>
		<?} */?>

		<div class="product-item-title">
			<div class="product-item-category"><?=$arProps["CATEGORY"]["VALUE"]?></div>
			<div class="product-item-name"><?=$item['NAME']?></div>
		</div>
		
		<div class="product-item-bottom-m d-block d-md-none">
			<?if (!empty($price)){?>
				<div class="product-item-price">
					от <span><?=number_format($price['RATIO_PRICE'],0,'.',' ')?></span> 
					<span class="rub">рублей</span>
				</div>
			<?}?>
			<a class="product-item-link" href="<?=$item['DETAIL_PAGE_URL']?>">Подробнее</a>
		</div>
		<div class="product-item-more d-block d-md-none">Подробнее о товаре</div>
		
		<div class="product-item-hover">
			<? //favorites 
			if( Bitrix\Main\Loader::includeModule('api.uncachedarea') ) {
					CAPIUncachedArea::includeFile(
						 "/favorites/include.php",
						 array(
								'ID' => $item['ID'],
								'LIST' => "Y",
						 )
					);
			}?>
			
			<div class="product-item-title">
				<div class="product-item-category"><?=$arProps["CATEGORY"]["VALUE"]?></div>
				<div class="product-item-name"><?=$item['NAME']?></div>
			</div>
			
			<div class="product-item-hover--col">
				<div class="product-item-hover--img">
					<img class="lazy" data-src="<?=$imgThumb["src"]?>" id="<?=$itemIds['PICT']?>">
				</div>
			</div>
			<div class="product-item-hover--col">
				<div class="product-item-hover--content">
					<!--div class="product-item-content-title">Подробнее о товаре</div-->
					<div class="row">
						<?if ($arProps["WIDTH"]["VALUE"] || $arProps["DEPTH"]["VALUE"] || $arProps["HEIGHT"]["VALUE"]){?>
						<div class="col-6 col-xl-5">
							<div class="product-item-prop-title">размер</div>
							<div class="product-item-prop">Ширина: <?=$arProps["WIDTH"]["VALUE"]?> мм</div>
							<div class="product-item-prop">Глубина: <?=$arProps["DEPTH"]["VALUE"]?> мм</div>
							<div class="product-item-prop">Высота: <?=$arProps["HEIGHT"]["VALUE"]?> мм</div>
						</div>
						<?}?>
						<div class="col-6">
							<div class="product-item-prop-title">материал</div>
							<div class="product-item-prop"> <!--span class="-material" style="background:#959EF8;"></span--> Обивка: Велюр</div>
							<div class="product-item-prop"> <!--span class="-material" style="background:#000;"></span--> Ножки: Массив бука</div>
						</div>
						<?if ($arProps["WEIGHT"]["VALUE"]){?>
						<div class="col-6 col-xl-5">
							<div class="product-item-prop-title">вес</div>
							<div class="product-item-prop"><?=$arProps["WEIGHT"]["VALUE"]?></div>
						</div>
						<?}?>
						<div class="col-6">
							<div class="product-item-prop-title">гарантия</div>
							<div class="product-item-prop">18 месяцев</div>
						</div>
					</div>
					<div class="product-item-hover-links">
						<a class="product-item-hover-link" href="<?=$item['DETAIL_PAGE_URL']?>">Подробнее о товаре</a>
						<?if ($arProps["AR_MODEL_LINK"]["VALUE"]){?>
							<a class="product-item-hover-link" href="/ar/?model=<?=$item["CODE"]?>" target="_blank">Примерить в интерьере</a>
						<?}?>
						<?if ($arProps["MODEL_3D_LINK"]["VALUE"]){?>
							<a class="product-item-hover-link" href="<?=$arProps["MODEL_3D_LINK"]["VALUE"]?>" target="_blank" rel="nofollow">Посмотреть 360</a>
						<?}?>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
	<div class="product-item-bottom d-none d-md-flex">
		<?if (!empty($price)){?>
			<div class="product-item-price" data-entity="price-block">
				от <span id="<?=$itemIds['PRICE']?>"><?=number_format($price['RATIO_PRICE'],0,'.',' ')?></span> 
				<span class="rub">рублей</span>
			</div>
		<?}?>
		<a class="product-item-link" href="<?=$item['DETAIL_PAGE_URL']?>">Подробнее</a>
	</div>

	


	<?//buttons?>
	<?/*
	<div data-entity="buttons-block">
		<?if ($actualItem['CAN_BUY']){?>
			<div id="<?=$itemIds['BASKET_ACTIONS']?>">
				<a class="btn btn-default" id="<?=$itemIds['BUY_LINK']?>"
					href="javascript:void(0)" rel="nofollow">
					<?=($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET'])?>
				</a>
			</div>
		<?}?>
	</div>
	*/?>
	
	<?
	//SCU
	/* if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $haveOffers && !empty($item['OFFERS_PROP'])){?>
		<div id="<?=$itemIds['PROP_DIV']?>">
			<?foreach ($arParams['SKU_PROPS'] as $skuProperty){
				$propertyId = $skuProperty['ID'];
				$skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
				if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
					continue;
				?>
				<div class="product-item-info-container product-item-hidden" data-entity="sku-block">
					<div class="product-item-scu-container" data-entity="sku-line-block">
						<?=$skuProperty['NAME']?>
						<div class="product-item-scu-block">
							<div class="product-item-scu-list">
								<ul class="product-item-scu-item-list">
									<?
									foreach ($skuProperty['VALUES'] as $value){
										if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
											continue;

										$value['NAME'] = htmlspecialcharsbx($value['NAME']);

										if ($skuProperty['SHOW_MODE'] === 'PICT'){?>
											<li class="product-item-scu-item-color-container" title="<?=$value['NAME']?>"
												data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
												<div class="product-item-scu-item-color-block">
													<div class="product-item-scu-item-color" title="<?=$value['NAME']?>"
														style="background-image: url('<?=$value['PICT']['SRC']?>');">
													</div>
												</div>
											</li>
										<?} else { ?>
											<li class="product-item-scu-item-text-container" title="<?=$value['NAME']?>"
												data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
												<div class="product-item-scu-item-text-block">
													<div class="product-item-scu-item-text"><?=$value['NAME']?></div>
												</div>
											</li>
											<?
										}
									}
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			<?}?>
		</div>
		<?
		foreach ($arParams['SKU_PROPS'] as $skuProperty){
			if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
				continue;

			$skuProps[] = array(
				'ID' => $skuProperty['ID'],
				'SHOW_MODE' => $skuProperty['SHOW_MODE'],
				'VALUES' => $skuProperty['VALUES'],
				'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
			);
		}

		unset($skuProperty, $value);

		if ($item['OFFERS_PROPS_DISPLAY']){
			foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer){
				$strProps = '';

				if (!empty($jsOffer['DISPLAY_PROPERTIES'])){
					foreach ($jsOffer['DISPLAY_PROPERTIES'] as $displayProperty){
						$strProps .= '<dt>'.$displayProperty['NAME'].'</dt><dd>'
							.(is_array($displayProperty['VALUE'])
								? implode(' / ', $displayProperty['VALUE'])
								: $displayProperty['VALUE'])
							.'</dd>';
					}
				}

				$item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
			}
			unset($jsOffer, $strProps);
		}
	} */
	?>