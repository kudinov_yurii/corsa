<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if (!empty($arResult["ITEMS"])){?>
<ul class="list-simple subnav-left-list">
	<?foreach($arResult["ITEMS"] as $arItem):?>
			<li class="nav-left-item">
				<a class="nav-left-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>" data-id="<?=$arItem["ID"]?>"><?=$arItem["NAME"]?></a>
			</li>
	<?endforeach;?>
</ul>
<?}?>