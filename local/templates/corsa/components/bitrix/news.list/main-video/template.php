<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if (!empty($arResult["ITEMS"])){?>
<div class="wrapper wrapper-full">
	<div class="index-steps">
		<?foreach($arResult["ITEMS"] as $k => $arItem):?>
			<div class="index-step">
				<div class="index-step-content">
					<div class="index-step-num"><?=++$k?></div>
					<div class="index-step-title"><?=$arItem["NAME"]?></div>
					<div class="index-step-text"><?=$arItem["PREVIEW_TEXT"]?></div>
				</div>
				<div class="index-step-img">
					<?if ($arItem["PROPERTIES"]["VIDEO"]["VALUE"]){?>
						<video src="<?=CFile::getPath($arItem["PROPERTIES"]["VIDEO"]["VALUE"])?>" poster="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" playsinline loop muted preload="none">
					<?}?>
				</div>
			</div>
		<?endforeach;?>
	</div>
</div>
<?}?>