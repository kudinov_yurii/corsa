<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if (!empty($arResult["ITEMS"])){?>
<div class="section-similar">
	<div class="wrapper">
		<?if ($arParams["BLOCK_TITLE"]){?>
			<div class="section-title"><?=$arParams["~BLOCK_TITLE"]?></div>
		<?}?>
		<div class="similar-list">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$thumb = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>285, 'height'=>285), BX_RESIZE_IMAGE_EXACT, false);
				$arPrice = CurrencyFormat($arItem['CATALOG_PRICE_1'], "RUB");
				$arPrice = str_replace(" &#8381;", " рублей", $arPrice);
				?>
				<div class="similar-item">
				<?if ($arItem["PREVIEW_PICTURE"]){?>
						<a class="similar-item-img" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img data-lazy="<?=$thumb["src"]?>" alt=""></a>
					<?}?>
					<a class="similar-item-name" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
					<?if ($arItem['CATALOG_PRICE_1']){?>
						<div class="similar-item-price"><?=$arPrice?></div>
					<?}?>
				</div>
			<?endforeach;?>
		</div>
	</div>
</div>
<?}?>