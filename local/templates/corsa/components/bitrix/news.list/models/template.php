<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if (!empty($arResult["ITEMS"])){?>
<div class="wrapper">
	<div class="products-list">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$arPrice = CurrencyFormat($arItem['CATALOG_PRICE_1'], "RUB");
			$arPrice = str_replace(" &#8381;", "", $arPrice);
			?>
			<div class="product-item-2">
				<a class="product-item-img-2" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
					<div class="product-item-title-2">
						<div class="product-item-category-2">стул</div>
						<div class="product-item-name-2"><?=$arItem["NAME"]?></div>
					</div>
				</a>
				<div class="product-item-info-2">
					<div class="product-item-price-2">от <span><?=$arPrice?></span> <span class="rub">рублей</span></div>
					<a class="product-item-link-2" href="<?=$arItem["DETAIL_PAGE_URL"]?>">подробнее</a>
				</div>
			</div>
		<?endforeach;?>
	</div>
</div>
<?}?>