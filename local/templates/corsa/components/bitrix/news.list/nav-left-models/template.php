<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$obParser = new CTextParser;
?>
<?if (!empty($arResult["ITEMS"])){?>
<div>	
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$arPrice = CurrencyFormat($arItem['CATALOG_PRICE_1'], "RUB");
		$arPrice = str_replace(" &#8381;", "", $arPrice);
		$arProps = $arItem["PROPERTIES"];
		?>
		<div class="nav-left-product" data-id="<?=$arItem["ID"]?>">
			<div class="index-slider-item">
				<div class="slider-warranty-title">Гарантия</div>
				<div class="slider-warranty-value">18 месяцев</div>
				<div class="slider-product-img">
					<?if ($arProps["SLIDER_PHOTO"]["VALUE"]){?>
						<img class="lazy" data-src="<?=CFile::getPath($arProps["SLIDER_PHOTO"]["VALUE"])?>">
					<?}?>
				</div>	
				<div class="slider-product-content">	
					<div class="slider-product-category"><?=$arProps["CATEGORY"]["VALUE"]?></div>
					<div class="slider-product-name"><?=$arItem["NAME"]?></div>
					<div class="slider-product-text"><?=$obParser->html_cut($arItem["PREVIEW_TEXT"], 200)?></div>
					<div class="slider-product-price">от <span><?=$arPrice?></span> рублей</div>
					<a class="slider-product-btn" href="<?=$arItem["DETAIL_PAGE_URL"]?>">подробнее</a>
				</div>
			</div>
		</div>
	<?endforeach;?>
</div>
<?}?>