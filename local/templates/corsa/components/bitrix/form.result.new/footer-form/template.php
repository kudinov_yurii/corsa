<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="footer-form">
	<div class="wrapper container-fluid">
		
<?if ($arResult["isFormNote"] != "Y"){?>
	<h2 class="section-title">задайте ваш вопрос</h2>
	<?//=$arResult["FORM_TITLE"]?>

 <?=$arResult["FORM_HEADER"]?>
	
	<?if ($arResult["isFormErrors"] == "Y"):?>
		<?//=$arResult["FORM_ERRORS_TEXT"];?>
	<?endif;?>

	<div class="row">
		<div class="col-md-5 col-lg-3 offset-lg-1">
		<?
			foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
			{
				if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
				{
					if ($FIELD_SID=="PAGE_URL"){
						$arQuestion["HTML_CODE"] = str_replace('value=""', 'value="'.$_SERVER["REQUEST_URI"].'"', $arQuestion["HTML_CODE"]);
					}
					echo $arQuestion["HTML_CODE"];
	
				}
				else
				{
					if ($FIELD_SID=="MESSAGE")
						continue;
					
					$class = "form__field field__".toLower($FIELD_SID);
					if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS']))
						$class .= ' field__error';
					
					$inputСaption=$arQuestion["CAPTION"]; 
					if ($arQuestion["REQUIRED"] == "Y")
						$inputСaption.=" *";
					$input = str_replace('name', 'placeholder="'.$inputСaption.'" name', $arQuestion["HTML_CODE"]);
					$input = str_replace('class="', 'class="'.$class.' ', $input);
			?>
				<div class="form__row">
					<?=$input?>	
				</div>	
			<?
				}
			} //endwhile
			?>
		</div>
		<div class="col-md-7 col-lg-7">
			<textarea placeholder="" name="form_textarea_4" class="form__field field__message"></textarea>
			
			<div class="form-btn-holder d-none d-md-block">
				<button class="form-btn" type="submit" value="<?=$arResult["arForm"]["BUTTON"]?>" name="web_form_submit">Отправить</button>
			</div>
			<label class="form__policy">
				<input type="checkbox" value="" required="" checked="">
				<span>Оставляя заявку, Вы соглашаетесь на обработку <a href="/policy/" target="_blank">персональных данных</a></span>
			</label>
			<div class="d-md-none">
				<input class="btn btn-block" type="submit" value="<?=$arResult["arForm"]["BUTTON"]?>" name="web_form_submit">
			</div>
		</div>	
			
	</div>		

	<?=$arResult["FORM_FOOTER"]?>


<?} else {?>
	 <div class="form-success">Заявка успешно отправлена.<br> В ближайшее время мы с Вами свяжемся.</div>
	 
	 <?if ($arResult["arForm"]["ID"]==1) {?>
		<script data-skip-moving="true">
			fbq('track', 'Schedule');
			console.log('fbq Schedule');
		</script>
	 <?}?>
	 
<?}?>
	</div>
</div><!-- /footer-form -->
