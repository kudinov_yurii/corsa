<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="wrapper">
	<div class="search-page">
		<form class="search-page-form" action="" method="get">
			<input class="search-page-input" type="text" name="q" value="<?=$arResult["REQUEST"]["QUERY"]?>">
			<input class="search-page-btn" type="submit" value="" />
		</form>

		<p class="search-query">Поиск по запросу <b>"<?=$arResult["REQUEST"]["QUERY"]?>"</b></p>
		
		<?if(is_object($arResult["NAV_RESULT"])):?>
			<p class="search-result"><?=GetMessage("CT_BSP_FOUND")?>: <b><?=$arResult["NAV_RESULT"]->SelectedRowsCount()?></b></p>
		<?endif;?>


		<div class="search-result">
		<?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
		<?elseif($arResult["ERROR_CODE"]!=0):?>
		<?elseif(count($arResult["SEARCH"])>0):?>
			
			<?foreach($arResult["SEARCH"] as $arItem):?>
				<div class="search-item">
					<h4><a href="<?echo $arItem["URL"]?>"><?echo $arItem["TITLE_FORMATED"]?></a></h4>
					<div class="search-preview"><?echo $arItem["BODY_FORMATED"]?></div>
				</div>
			<?endforeach;?>
			
			<?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"]?>

		<?else:?>
			<?ShowNote(GetMessage("CT_BSP_NOTHING_TO_FOUND"));?>
		<?endif;?>

		</div>
	</div>
</div>