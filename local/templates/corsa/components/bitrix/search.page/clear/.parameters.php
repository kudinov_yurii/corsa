<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arTemplateParameters = array(
	"USE_SUGGEST" => Array(
		"NAME" => GetMessage("TP_BSP_USE_SUGGEST"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "N",
	),
	"SHOW_ITEM_TAGS" => array(
		"NAME" => GetMessage("TP_BSP_SHOW_ITEM_TAGS"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
		"REFRESH" => "Y",
	),
	"TAGS_INHERIT" => array(
		"NAME" => GetMessage("TP_BSP_TAGS_INHERIT"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"SHOW_ITEM_DATE_CHANGE" => array(
		"NAME" => GetMessage("TP_BSP_SHOW_ITEM_DATE_CHANGE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"SHOW_ORDER_BY" => array(
		"NAME" => GetMessage("TP_BSP_SHOW_ORDER_BY"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"SHOW_TAGS_CLOUD" => array(
		"NAME" => GetMessage("TP_BSP_SHOW_TAGS_CLOUD"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "N",
		"REFRESH" => "Y",
	),
);

if($arCurrentValues["SHOW_ITEM_TAGS"] == "N")
	unset($arTemplateParameters["TAGS_INHERIT"]);

?>
