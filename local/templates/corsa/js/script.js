$(function() {
	if ( typeof($.fn.lazy)!=='undefined' ){
		$('.lazy').Lazy();
	}	
});

$(document).ready(function () {
	
	if ( typeof($.fn.slick)!=='undefined' ){
		
		$('.index-slider').slick({
			accessability: false,
			arrows: false,
			dots: true,
			autoplay: false,
			pauseOnHover: false,
			autoplaySpeed: 5000,
			speed: 1000,
			infinite:true,  
			adaptiveHeight:true, 
		});
		
		$('.product-slider').slick({
			accessability: false,
			arrows: true,
			dots: true,
			autoplay: false,
			pauseOnHover: false,
			autoplaySpeed: 5000,
			speed: 1000,
			infinite:true,  
			adaptiveHeight:true, 
			asNavFor: '.product-slider-preview'
		});
		$('.product-slider-preview').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.product-slider',
			dots: false,
			arrows: false,
			focusOnSelect: true,
			vertical: true,
			responsive: [
				{
					breakpoint: 1201,
					settings: {
						vertical: false,
					}
				},
			]
		});
		
		
		$('.bloger-list').slick({
			accessability: false,
			slidesToShow: 3,
			slidesToScroll: 1,
			dots: false,
			arrows: false,
			responsive: [
				{
					breakpoint: 575,
					settings: {
						slidesToShow: 2,
						variableWidth: true,
					}
				}
			]
		});
		
		$('.configuration-list').slick({
			accessability: false,
			slidesToShow: 7,
			slidesToScroll: 1,
			dots: false,
			arrows: false,
			responsive: [
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 5,
					}
				},
				{
					breakpoint: 575,
					settings: {
						slidesToShow: 2,
						variableWidth: true,
					}
				}
			]
		});
		
		$('.similar-list').slick({
			lazyLoad: 'ondemand',
			accessability: false,
			slidesToShow: 6,
			slidesToScroll: 2,
			dots: false,
			arrows: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 575,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						variableWidth: true,
					}
				}
			]
		});
		
		
		if (window.matchMedia("(max-width: 575px)").matches) {
			$('.interior-list').slick({
				accessability: false,
				slidesToShow: 2,
				slidesToScroll: 1,
				dots: false,
				arrows: false,
				variableWidth: true //
			});
		}
		
	} //if slick
	
	if ( typeof($.fn.fancybox)!=='undefined' ){
		$('[data-fancybox][data-src*="#"]').fancybox({
			touch: false
		});
	}
	
	if ( typeof($.fn.mask)!=='undefined' ){
		$('input[name="phone"], input[name="properties[PHONE]"], .field__phone').mask("+7 (999) 999-99-99");
		if ( typeof(BX)!=='undefined' ){
			BX.addCustomEvent('onAjaxSuccess', function(){
				$('input[name="phone"], input[name="properties[PHONE]"], .field__phone').mask("+7(999) 999-99-99");
			});
		}
	}
	
	// video scrolling play
	$(function() {
		var $video = $('video');
		var $window = $(window);

		if ($video.length==0)
			return;
		
		$video.each(function () {
			setPlay($(this))
		});

		function setPlay(video) {
			$window.scroll(function() {
				var $topOfVideo = video.offset().top;
				var $bottomOfVideo = video.offset().top + video.outerHeight();

				var $topOfScreen = $window.scrollTop();
				var $bottomOfScreen = $window.scrollTop() + $window.innerHeight();
				
				if(($bottomOfScreen > $bottomOfVideo) && ($topOfScreen < $topOfVideo)){
					video[0].play();
				} else {
					video[0].pause();
				}	
			});
		}
		
	});
	
	$(".nav-left-link[data-id]").hover(
		function () {
			$('.nav-left-product').hide();
			$('.nav-left-product[data-id="'+$(this).data('id')+'"]').show();
			$('.nav-left').addClass('-bg');
		},
		function () {
			//$('.nav-left-product').hide();
		}
	);
	


	$('.js-modal-link').on('click', function() {
		var modal = $(this).data('modal');
		if ( $('.js-modal[data-modal="'+modal+'"]').length ){
			$('.js-modal-inner').append('<span class="js-modal-close"></span>');
			$('.js-modal[data-modal="'+modal+'"]')
			.css("display", "flex")
			.hide()
			.fadeIn();
			$('body').addClass('modal-active');
		}
		return false;
	});
	$('body').on('click', '.js-modal-close', function() {
		closeModal();
	});
	// Закрываем по ESC
	$(document).keyup(function(e) {
		if (e.which == 27) {
			closeModal();
		}
	});
	$(document).mouseup(function (e) {
		var container = $(".js-modal");
		if (container.has(e.target).length === 0){
			closeModal();
		}
	});
	function closeModal(){
		$('.js-modal').fadeOut();
		$('body').removeClass('modal-active');
		return false;
	}

	$('.btn-basket').on('click', function() {
		var img = $('[data-img-src]').data('img-src');
		var price = $('.product-price').text();
		var category = $('.product-category').text();
		var name = $('.product-title').text();
		var upholsteryName = $('.upholstery-item.selected[data-type="upholstery"]').data('name');
		var upholsteryImg = $('.upholstery-item.selected[data-type="upholstery"]').data('img');
		var legsName = $('.upholstery-item.selected[data-type="legs"]').data('name');
		var legsImg = $('.upholstery-item.selected[data-type="legs"]').data('img');

		$('.popup-basket-category').text(category);	
		$('.popup-basket-name').text(name);	
		$('.popup-basket-price').text(price+' рублей');	
		$('.popup-basket-img img').attr('src', img);
		if (typeof(upholsteryName)!=='undefined'){
			$('.popup-basket-material[data-type="upholstery"]').html('<span style="background-image:url('+upholsteryImg+');"></span> Обивка - '+upholsteryName+'');
		}
		if (typeof(legsName)!=='undefined'){
			$('.popup-basket-material[data-type="legs"]').html('<span style="background-image:url('+legsImg+');"></span> Ножки - '+legsName+'');
		}
		
		showBasketModal();
		
		return false;
	});
	
	function showBasketModal(){
		$.fancybox.open({
			src  : '#popup-basket',
		});
	}
	
	$('.tab-btn').on('click', function() {
		var tab = $(this).data('tab');
		$(".tab-btn, .tab-content").removeClass("active");
		$(this).addClass("active");
		$(".tab-content[data-tab='"+tab+"']").addClass("active");
		return false;
	});
	
	$('[data-toggler]').on('click', function() {
		var id = $(this).data('toggler');
		$('[data-toggle-text="'+id+'"]').slideToggle();
		return false;
	});
	
	$('.header-icon-search').on('click', function() {
		$('.header-search-form').toggleClass('-opened');
		return false;
	});

	
	
	$('.nav-left-toggle').on('click', function() {
		$(this).toggleClass('-opened');
		$('.nav-left').toggleClass('-opened');
		return false;
	});
	
	$('.nav-toggle').on('click', function() {
		$(this).toggleClass('-opened');
		$('.navigation-list').toggleClass('-opened');
		return false;
	});
	
	$('.catalog-filter-toggle').on('click', function() {
		$('.catalog-filter').slideToggle();
		return false;
	});
	
	$('.filter-row-name').on('click', function() {
		$(this).toggleClass('-opened');
		$(this).siblings('.filter-row-content').slideToggle();
		return false;
	});
	
	$('.product-item-more').on('click', function() {
		$(this).toggleClass('-opened');
		$(this).siblings('.product-item-hover').toggleClass('-opened');;
		return false;
	});
	
	
	$('.bloger-item').on('click', function() {
		var id = $(this).data('id');
		$('.bloger-block').removeClass('-active');
		$('.bloger-block[data-id="'+id+'"]').addClass('-active');
		return false;
	});
	
	$('.product-text-show').on('click', function() {
		var id = $(this).data('id');
		$('.product-text-hidden[data-id="'+id+'"]').toggleClass('-opened');
		return false;
	});
	
	$('.upholstery-item').on('click', function() {
		var img = $(this).data('img');
		var type = $(this).data('type');
		$('.upholstery-item').removeClass('-selected');
		$(this).addClass('-selected');
		$('.product-material-img[data-type="'+type+'"]')
			.addClass('-selected')
			.css('background-image', 'url(' + img + ')');
			
		if ( 
			$('.product-material-img[data-type="upholstery"]').hasClass('-selected') && 
			$('.product-material-img[data-type="legs"]').hasClass('-selected') 
		){
			$('.product-btns .btn-basket').prop('disabled', false);
		}	
		return false;
	});
	
	$('.product-info-name i').on('click', function() {
		$(this).toggleClass('-closed');
		$(this).parent().siblings('.product-info-inner').slideToggle();
		return false;
	});
	
	
	$('input[data-delivery-type]').on('change', function() {
		showDeliveryBlock( $(this) );
	});
	
	$('input[data-delivery-type]').each(function () {
		showDeliveryBlock( $(this) );
	});
	

	if ( typeof(BX)!=='undefined' ){
		BX.addCustomEvent('onAjaxSuccess', function(){
			$('input[data-delivery-type]').each(function () {
				showDeliveryBlock( $(this) );
			});
			$('input[data-delivery-type]').on('change', function() {
				showDeliveryBlock( $(this) );
			});
		});
	}
	
	function showDeliveryBlock(input){
		var type = $(input).data('delivery-type');
		if ($(input).is(':checked')) {
			$('.delivery-type-block').hide();
			$('.delivery-type-block[data-delivery-type="'+type+'"]').show();
		}
	}
	
	
	$('.footer-models-title').on('click', function() {
		$('.footer-models').slideToggle();
		return false;
	});
	

	
});

