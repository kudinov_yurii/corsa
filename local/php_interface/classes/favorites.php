<?
namespace Favorites;

/***
structure
UF_USER_ID (int)
UF_COOKIE  (string)
UF_ELEMENT_ID (int)
UF_DATE  (date)
***/

class FavoritesManager {
	
	protected $table = false;
	protected static $cookie_user_id;
	protected static $hl_id = 6; // Highloadblock ID
	
	function __construct(){
		\Bitrix\Main\Loader::includeModule('highloadblock');
		$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById( self::$hl_id )->fetch();
		$entity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $hlblock );
		$entity_data_class = $entity->getDataClass();
		$this->table = $entity_data_class;
	}
	
 	public static function getCookieUserID(){
		global $APPLICATION;
		if(isset(self::$cookie_user_id)){
			return self::$cookie_user_id;
		}
		if($cookie_user_id = $APPLICATION->get_cookie("COOKIE_USER_ID")){
			return $cookie_user_id;
		}else{
			$cookie_user_id = md5(time().randString(5));
			$APPLICATION->set_cookie("COOKIE_USER_ID", $cookie_user_id, time()+60*60*24*30*3); //3 month
			self::$cookie_user_id = $cookie_user_id;
			return $cookie_user_id;
		}

	} 	
	
	public function getUser(){
		global $USER;
		
		if ($USER->IsAuthorized()){
			
			$listWithoutAuthUser = $this->listWithoutAuthUser();
			if ( count($listWithoutAuthUser)>0 ){
				foreach ( $listWithoutAuthUser as $k => $v ){
					$this->table::update($k, array('UF_USER_ID' => $USER->GetID() ) );
				}
			}
			return $USER->GetID();
			
		} else {
			return self::getCookieUserID();
		}
		
	}
	
	public function typeFiltration(){
		global $USER;
		if ($USER->IsAuthorized()){
			return "USER_ID";
		} else {
			return "COOKIE";
		}
		
	}
	
	public function listWithoutAuthUser(){
		$result = array();
			$arParams = array(
				'filter' => array(
				 'UF_USER_ID' => "",
				 'UF_COOKIE' => self::getCookieUserID(),
				),
				'order' => array(
					'ID' => 'DESC',
				),
			);
			$rsData = $this->table::getList($arParams);
			while($arRes = $rsData->fetch()){
				$result[$arRes["ID"]] = $arRes["UF_ELEMENT_ID"];
			}
		return $result;
	}	
	
	public function getIdList(){
		$result = array();
		$arParams = array(
			'filter' => array(
			"UF_{$this->typeFiltration()}" => $this->getUser(),
			),
			'order' => array(
				'ID' => 'DESC',
			),
		);
		$rsData = $this->table::getList($arParams);
		while($arRes = $rsData->fetch()){
			$result[] = $arRes["UF_ELEMENT_ID"];
		}
		return $result;
	}
	
	public function add($ID){
		$result = false;
		if(!empty($ID) && $ID > 0){
			$arParams = array(
				"UF_{$this->typeFiltration()}" => $this->getUser(),
				'UF_ELEMENT_ID' => $ID,
				'UF_DATE' => new \Bitrix\Main\Type\DateTime(),
			);
			$res = $this->table::add($arParams);
			
			if($res->isSuccess()){
				$result = $res->getId();
			}
		}
		return $result;
	}
	
	public function remove($ID){
		$result = false;
		if(!empty($ID) && $ID > 0){
			if($nRowID = $this->check($ID)){
				$res = $this->table::delete($nRowID);
				if($res->isSuccess()){
					$result = true;
				}
			}
		}
		return $result;
	}
	
	public function check($ID){
		$result = false;
		if(!empty($ID) && $ID > 0){
			$arParams = array(
				'filter' => array(
					"UF_{$this->typeFiltration()}" => $this->getUser(),
					'UF_ELEMENT_ID' => $ID,
				),
				'order' => array("ID" => "ASC"),
				'limit' => 1
			);
			$rsData = $this->table::getList($arParams);
			if($arRes = $rsData->fetch()){
				$result = $arRes['ID'];
			}
		}
		return $result;
	}
	
	
}

?>