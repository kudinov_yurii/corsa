<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::AddAutoloadClasses(
	'', // не указываем имя модуля
	array(
		'\Favorites\FavoritesManager' => '/local/php_interface/classes/favorites.php',
	)
);

require_once 'include/Mobile_Detect.php';
$detect = new Mobile_Detect;
$isMobile = ( $detect->isMobile() ? true : false );

// пользовательские поля для разделов
function GetSectionField($iblock_id, $section_id, $uf_id) {
	$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields ("IBLOCK_".$iblock_id."_SECTION", $section_id);
	return $arUF[$uf_id]["VALUE"];
}

function getUTM(){
	foreach($_REQUEST as $key => $val) {
		if(preg_match('/utm_/',$key)) {
			setcookie($key, $val, time()+86400, "/");
		}
	}
	if(!preg_match('/corsa-home\.ru/', $_SERVER['HTTP_REFERER'])) {
			$tmp = explode('?', $_SERVER['HTTP_REFERER']);
			setcookie('referer', $tmp[0], time()+86400, "/");
	}

	$massUTM = array('utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term');
	$utm = '';	
	foreach($massUTM as $val) {     
		$utm .= "\n".$val.': '.$_COOKIE[$val];       
	}
	if(isset($_COOKIE['referer']) && $_COOKIE['referer'] != '') {
		$utm .= "\nНа сайт перешли с внешнего ресурса - " . $_COOKIE['referer'];
	}
	return $utm;
}
getUTM();

// utm-метки в почтовый шаблон
AddEventHandler('form', 'onBeforeResultAdd', 'onBeforeResultAddHandler');
function onBeforeResultAddHandler($WEB_FORM_ID, &$arFields, &$arrVALUES){
	if ($WEB_FORM_ID == 1) {
		$arrVALUES['form_hidden_6'] = getUTM();	
  }
}

// utm-метки в письмо о новом заказе
AddEventHandler("sale", "OnOrderNewSendEmail", "bxModifySaleMails");
function bxModifySaleMails($orderID, &$eventName, &$arFields){
	$arFields["UTM"] = getUTM();
}


// отложенная функция для вывода h1
function ShowH1(){
  global $APPLICATION;
	$hide_h1 = $APPLICATION->GetPageProperty("hide-h1");
	
	if ( $hide_h1=="Y" ){
    return false;
  } else {
		$h1 = $APPLICATION->GetPageProperty("h1");
		if( empty($h1) ){
			$h1 = $APPLICATION->GetTitle(false);
		}
		return "<h1 class='h1 text-center'>".$h1."</h1>";
	}
}

function curlConnect($method, $params=array(), $debug=false){
	if (!empty($method)){
		
		if ($debug) {
        echo "<pre>"; print_r($params); echo "</pre>";
        $result = array(time() => 'added');
				
    } else { 
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://magic-trans.ru/api/v1/'.$method);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

			$result = curl_exec($ch);
			$result = json_decode($result, 1);
			curl_close($ch);
			
		}
		return $result;
	}
}


use Bitrix\Main;
Main\EventManager::getInstance()->addEventHandler(
	'sale',
	'OnSaleOrderBeforeSaved',
	'OnSaleOrderBeforeSavedHandler'
);


function OnSaleOrderBeforeSavedHandler(Main\Event $event) {
	
	/** @var \Bitrix\Sale\Order $order */
	$order = $event->getParameter("ENTITY");

	/** @var \Bitrix\Sale\PropertyValueCollection $propertyCollection */
	$propertyCollection = $order->getPropertyCollection();

	$propsData = [];

	/**
	 * Собираем все свойства и их значения в массив
	 * @var \Bitrix\Sale\PropertyValue $propertyItem
	 */
	foreach ($propertyCollection as $propertyItem) {
			if (!empty($propertyItem->getField("CODE"))) {
					$propsData[$propertyItem->getField("CODE")] = trim($propertyItem->getValue());
			}
	}
	
	/**
	 * Проверяем пользователя по e-mail 
	 * Если есть - прикрепляем ему созданный заказ
	 * Если нет - анонимно регистрируем нового и также прикрепляем ему заказ
	 */
	global $USER;
	if (!$USER->IsAuthorized()){
		
		if (strlen($propsData["EMAIL"]) > 0) {
				$res = \Bitrix\Main\UserTable::getRow(array(
						'filter' => array(
								"=EMAIL" => $propsData["EMAIL"],
								"EXTERNAL_AUTH_ID" => ''
						),
						'select' => array('ID')
				));

				if (intval($res["ID"]) > 0) {
						$userID = intval($res["ID"]);
						
				} else {
						$randPass = randString(8);
						$arFields = Array(
							"NAME"              => $propsData["FIO"],
							"EMAIL"             => $propsData["EMAIL"],
							"LOGIN"             => $propsData["EMAIL"],
							"PERSONAL_PHONE" 		=> $propsData['PHONE'],
							"ACTIVE"            => "N",
							"GROUP_ID"          => array(3),
							"PASSWORD"          => $randPass,
							"CONFIRM_PASSWORD"  => $randPass,
						);
						$user = new CUser;
						$userID = $user->Add($arFields);							
			}
			
			if (intval($userID) > 0){
				$order->setFieldNoDemand('USER_ID', $userID);
			}


		}
		
	}
	
	//AddMessage2Log('$propsData = '.print_r($propsData, true).'');

}

?>