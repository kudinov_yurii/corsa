<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("title", "Страница 404");
$APPLICATION->SetTitle("Ошибка 404");
//$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>

<div class="wrapper">
	<p>По адресу <i><script data-skip-moving="true">document.write(location.href)</script></i> ничего нет.</p>
	<p><strong>Причины, которые могли привести к ошибке:</strong></p>
	<ul>
		<li>Неправильно набран адрес.</li>
		<li>Такой страницы никогда не было на этом сайте.</li>
		<li>Страница была удалена.</li>
	</ul>
	<p><a href="/">Вернуться на главную страницу</a></p>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>