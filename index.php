<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Главная");
$APPLICATION->SetPageProperty("hide-h1", "Y");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>

<?$GLOBALS['arrFilterSlider'] = array("!PROPERTY_SHOW_IN_SLIDER"=>false);?>
<?$APPLICATION->IncludeComponent("bitrix:news.list","slider-models",Array(
	"IS_MOBILE" => $isMobile,
	"FILTER_NAME" => "arrFilterSlider",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"AJAX_MODE" => "N",
	"IBLOCK_TYPE" => "",
	"IBLOCK_ID" => '1',
	"NEWS_COUNT" => "40",
	"SORT_BY1" => "CATALOG_PRICE_1",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "ASC",
	"FIELD_CODE" => Array("CATALOG_PRICE_1"),
	"PROPERTY_CODE" => Array("CATEGORY"),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "N",
	"SET_BROWSER_TITLE" => "N",
	"SET_META_KEYWORDS" => "N",
	"SET_META_DESCRIPTION" => "N",
	"SET_LAST_MODIFIED" => "Y",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"INCLUDE_SUBSECTIONS" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000",
	"CACHE_FILTER" => "Y",
	"CACHE_GROUPS" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"SET_STATUS_404" => "N",
	"SHOW_404" => "N",
	"MESSAGE_404" => "",
	"PAGER_BASE_LINK" => "",
	"PAGER_PARAMS_NAME" => "arrPager",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
)
);?>

<?$APPLICATION->IncludeFile(SITE_DIR."include/index/block-interior.php", Array(), Array("SHOW_BORDER"=>true));?>

<?$APPLICATION->IncludeFile(SITE_DIR."include/index/own-production.php", Array(), Array("SHOW_BORDER"=>true));?>

<?$APPLICATION->IncludeFile(SITE_DIR."include/index/our-projects.php", Array(), Array("SHOW_BORDER"=>true));?>


<?$APPLICATION->IncludeComponent("bitrix:news.list","main-video",Array(
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"AJAX_MODE" => "N",
	"IBLOCK_TYPE" => "",
	"IBLOCK_ID" => '3',
	"NEWS_COUNT" => "10",
	"SORT_BY1" => "SORT",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "ID",
	"SORT_ORDER2" => "ASC",
	"FIELD_CODE" => Array(""),
	"PROPERTY_CODE" => Array("VIDEO"),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "N",
	"SET_BROWSER_TITLE" => "N",
	"SET_META_KEYWORDS" => "N",
	"SET_META_DESCRIPTION" => "N",
	"SET_LAST_MODIFIED" => "Y",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"INCLUDE_SUBSECTIONS" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000",
	"CACHE_FILTER" => "Y",
	"CACHE_GROUPS" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"SET_STATUS_404" => "N",
	"SHOW_404" => "N",
	"MESSAGE_404" => "",
	"PAGER_BASE_LINK" => "",
	"PAGER_PARAMS_NAME" => "arrPager",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
)
);?>


<!--div class="wrapper wrapper-full">
	<div class="trust-us">
		<h2 class="section-title">Нам доверяют</h2>
		<div class="bloger-list">
			<div class="bloger-item" data-id="1">
				<div class="bloger-item-img"></div>
				<div class="bloger-item-name">bloger 1</div>
			</div>
			<div class="bloger-item" data-id="2">
				<div class="bloger-item-img"></div>
				<div class="bloger-item-name">bloger 2</div>
			</div>
			<div class="bloger-item" data-id="3">
				<div class="bloger-item-img"></div>
				<div class="bloger-item-name">bloger 3</div>
			</div>
		</div>
	</div>
</div>

<div class="wrapper wrapper-full">
	<div class="bloger-block -active" data-id="1">
		<div class="bloger-img"><img src="i/bloger.jpg" alt=""></div>
		<div class="bloger-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labo</div>
	</div>
	<div class="bloger-block" data-id="2">
		<div class="bloger-img"><img src="i/bloger.jpg" alt=""></div>
		<div class="bloger-text">===222=== Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labo</div>
	</div>
	<div class="bloger-block" data-id="3">
		<div class="bloger-img"><img src="i/bloger.jpg" alt=""></div>
		<div class="bloger-text">===333=== Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labo</div>
	</div>
</div-->


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>