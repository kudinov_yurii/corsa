<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("CorsaStyle - российская дизайнерская мебель");

?>
<div>
	<div class="own-production -inverse">
		<div class="own-production-img"><img class="lazy" data-src="/upload/images/about-1.jpg"></div>
		<div class="own-production-block -bg">
			<div class="own-production-title">Кто мы?</div>
			<p>C 2011 года фабрика «CorsaStyle» производит дизайнерскую мебель для сегмента Horeca. Мы постоянно растём и стремимся покорять новые вершины. Начав с небольшой мастерской, мы выросли в современное производство площадью 2200 м<sup>2</sup>. И в 2020 году мы решили открыть новое направление Corsa Style Home — стильные стулья для вашего дома. Наша цель — чтобы у вас была красивая, при этом комфортная и долговечная мебель.</p>
		</div>
		<div class="own-production-block">
			<div class="own-production-title">Почему мы?</div>
			<p>«CorsaStyle» — это команда профессионалов, которая стремится создавать лучший товар здесь и сейчас. Креативность, гибкость и надежность — это про нас. Мы быстро реагируем на любые изменения и в нашем ассортименте стулья, соответствующие современным тенденциям в дизайне мебели. </p>
		</div>
		 
	</div>
</div>
		
<div class="page-about">

<div class="row">
	<div class="col-md-5 offset-md-1">
		<img class="about-block-img" src="<?=SITE_TEMPLATE_PATH?>/i/svg/about-1.svg" alt="">
		<div class="about-block-title">Опыт работы</div>               
		<p class="about-block-text">практически во всех уголках России есть наша мебель, за 9 лет мы выполнили более 500 проектов для гостиниц и ресторанов. Вы можете быть уверены, что у нас есть знания и опыт, чтобы делать удобную, качественную мебель и для вашего дома.</p>
	</div>
	<div class="col-md-5">
		<img class="about-block-img" src="<?=SITE_TEMPLATE_PATH?>/i/svg/about-2.svg" alt="">
		<div class="about-block-title">Материалы и знание продукта</div>                  
		<p class="about-block-text">мы тесно сотрудничаем с нашими поставщиками, чтобы знать всё, что можно, о материалах и деталях, из которых состоит наша мебель. Поездки в Европу, обучение персонала, внимательный подбор поставщиков материалов, делают нас экспертами в нашей области.</p>
	</div>
	<div class="col-md-5 offset-md-1">
		<img class="about-block-img" src="<?=SITE_TEMPLATE_PATH?>/i/svg/about-3.svg" alt="">
		<div class="about-block-title">Высококвалифицированный персонал</div>                 
		<p class="about-block-text">все сотрудники компании «Corsastyle» регулярно проходят обучение, изучают новейшие технологии и материалы. Благодаря регулярным визитам наших постоянных поставщиков наш персонал всегда в курсе всех последних тенденций и методов производства, именно поэтому наши стулья такие прочные и удобные. </p>
	</div>
	<div class="col-md-5">
		<img class="about-block-img" src="<?=SITE_TEMPLATE_PATH?>/i/svg/about-4.svg" alt="">
		<div class="about-block-title">Производство</div>                     
		<p class="about-block-text">наше производство оснащено новейшим промышленным оборудованием ведущих европейских брендов. Комплексная система контроля качества действует на всех этапах производства. Производственная база в Самаре позволяет нам каждую минуту знать, что происходит с Вашим заказом — на каком он этапе.</p> 
	</div>
	
	<div class="col-md-5 offset-md-1">                  
		<p class="about-block-text">Остались вопросы? Свяжитесь с нами!</p> 
	</div>
</div>

<div class="row">
	<div class="col-sm-6 col-md-5 offset-md-1">
		<a data-fancybox="gallery" href="/upload/images/about-2.jpg"><img style="margin-bottom:30px;" class="lazy" data-src="/upload/images/about-2.jpg" alt=""></a>
		<a data-fancybox="gallery" href="/upload/images/about-3.jpg"><img style="margin-bottom:30px;" class="lazy" data-src="/upload/images/about-3.jpg" alt=""></a>
	</div>
	<div class="col-sm-6 col-md-5">
		<a data-fancybox="gallery" href="/upload/images/about-4.jpg"><img style="margin-bottom:30px;" class="lazy" data-src="/upload/images/about-4.jpg" alt=""></a>
		<a data-fancybox="gallery" href="/upload/images/about-5.jpg"><img style="margin-bottom:30px;" class="lazy" data-src="/upload/images/about-5.jpg" alt=""></a>
	</div>


</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>